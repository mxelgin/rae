<?php
/**
 * Base overrides for frontend application
 */
return [
    // So our relative path aliases will resolve against the `/frontend` subdirectory and not nonexistent `/protected`
    'basePath' => 'frontend',
    'name'=>'ТюмГМА',
    'theme' => 'rae2',
    'preload' => array('bootstrap'),
    'import' => [
        'common.actions.*',
        'application.controllers.*',
        'application.controllers.actions.*',
        'application.models.*',
        'application.components.*',
        'application.models.validators.*',
        'application.modules.message.*',
        'application.modules.message.models.*',
        'application.modules.user.models.*',
        'application.modules.user.components.*',
        'application.modules.rights.*',
        'application.modules.rights.components.*',
        'ext.giix-components.*', // giix components
    ],
    'controllerMap' => [
        // Overriding the controller ID so we have prettier URLs without meddling with URL rules
        'site' => 'FrontendSiteController'
    ],
    'components' => [
        // uncomment the following to enable URLs in path-format
        'errorHandler' => [
            // Installing our own error page.
            'errorAction' => 'site/error'
        ],
        'urlManager' => [
            // Some sane usability rules
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',

                // Your other rules here...
            ]
        ],
        'bootstrap' => array(
            'class' => 'bootstrap.components.Bootstrap',
        ),
    ],
    'params'=>array(
        // this is used in contact page
        'adminEmail'=>'mxelgin@yandex.ru',
    ),
    'sourceLanguage' => 'en',
    'language' => 'ru',
];