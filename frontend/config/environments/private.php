<?php
/**
 * Specific config overrides for frontend entry point at local development workstations.
 */
return [
    // Normally you don't have anything to specify for frontend entry point at local workstations.
    'components' => [
        'db' => [
            'connectionString' => 'mysql:host=192.168.1.111;dbname=c3rae',
            'username' => 'c3rae',
            'password' => 'c3rae',
        ]
    ],
];

