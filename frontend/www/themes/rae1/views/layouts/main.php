<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
<!--	<link rel="stylesheet" type="text/css" href="--><?php //echo Yii::app()->request->baseUrl; ?><!--/css/screen.css" media="screen, projection" />-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/ckeditor/content.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/molcenter.css" />


	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<!--    --><?php
//
//    $this->widget('bootstrap.widgets.TbNavbar', array(
//        'type'=>null, // null or 'inverse'
//        'brand'=>'<img src="'. Yii::app()->theme->baseUrl. '/img/logo.png"/>',
//        'brandUrl'=>'/',
//        'htmlOptions' => array('class' => 'nav-fixed'),
////        'collapse'=>true, // requires bootstrap-responsive.css
//        'items'=>array(
//
////            array(
////                'class'=>'bootstrap.widgets.TbLabel',
////                'encodeLabel' => false,
////                'label'=>'<a href="/user/logout"><i class="icon-off icon-white"></i></a>',
////                'htmlOptions'=>array('class'=>'pull-right nav-text '.  (Yii::app()->user->isGuest ? 'hidden' : ''))
////            ),
////            array(
////                'class'=>'bootstrap.widgets.TbLabel',
////                'encodeLabel' => false,
////                'label'=>'<i class="icon-user icon-white"></i> Вы вошли как '. (Yii::app()->user->isGuest? 'гость' : Yii::app()->user->name) ,
////                'htmlOptions'=>array('class'=>'pull-right nav-text')
////            ),
////            array(
////                'class'=>'bootstrap.widgets.TbLabel',
////                'encodeLabel' => false,
////                'label'=>'<a href="/message"><i class="icon-envelope icon-white"></i> Сообщений ('. Message::model()->getCountUnreaded(Yii::app()->user->id) . ')</a>' ,
////                'htmlOptions'=>array('class'=>'pull-right nav-text text-green '.  (Yii::app()->user->isGuest ? 'hidden' : ''))
////            ),
////            array(
////                'class'=>'bootstrap.widgets.TbMenu',
////                'items'=>array(
////                        array('label'=>'Login', 'url'=>'#', 'visible'=>!Yii::app()->user->isGuest, 'items'=>array(
////                        array('label'=>'Login', 'url'=>array('/user/login'), 'visible'=>Yii::app()->user->isGuest),
////                        array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/user/logout'), 'visible'=>!Yii::app()->user->isGuest)
////
////                    )),
////                ),
////            ),
//        ),
//    ));
//
//    /*
//    $this->widget('zii.widgets.CMenu',array(
//        'items'=>array(
////				array('label'=>'Справочники', 'url'=>array('/directory')),
////				array('label'=>'Панель управления', 'url'=>array('/adminPanel')),
////				array('label'=>'Настройки', 'url'=>array('/adminPanel')),
////				array('label'=>'Пользователи', 'url'=>array('/adminPanel')),
//            array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
//            array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
//        ),
//    ));*/ ?>
<!-- mainmenu -->

<!--<div class="row-fluid row-breadcrumbs">-->
<!--    <div class="span12">-->
<!--        --><?php //if(isset($this->breadcrumbs)):?>
<!--            --><?php //$this->widget('zii.widgets.CBreadcrumbs', array(
//                'links'=>$this->breadcrumbs,
//            )); ?>
<!--             -->
<!--        --><?php //endif?>
<!--    </div>-->
<!--</div>-->

<div class="container container_main" id="page">
    <div class="row-fluid row_header"></div>
    <!--div id="header"-->
        <!--		<div id="logo">--><?php //echo CHtml::encode(Yii::app()->name); ?><!--</div>-->
    <!--/div--><!-- header -->




<!--    --><?php //if (!empty ($this->submenu)):?>
<!--        <div id="submenu">-->
<!--            --><?php //foreach($this->submenu as $title=>$url):?>
<!--                <a href="--><?//=$url?><!--">--><?//=$title?><!--</a>-->
<!--            --><?php //endforeach;?>
<!--        </div>-->
<!--    --><?php //endif;?>


 <div class="row-fluid">

     <?php

     $this->widget(
         'bootstrap.widgets.TbNavbar',
         array(
             'brand' => false,
             'fixed' => false,
             'items' => array(
                 array(
                     'class' => 'bootstrap.widgets.TbMenu',
                     'items' => array(
                         array('label' => 'О журнале', 'url' => '/'),
                         array('label' => 'Номера журнала', 'url' => '/issue'),
                         array('label' => 'Поиск', 'url' => '/search'),
                         array('label' => 'Правила', 'url' => '/rules'),
                         array('label' => 'Колонка редактора', 'url' => '/news'),
                     )
                 )
             )
         )
     );

     ?>


         <div class=" row-breadcrumbs">

                 <?php if(isset($this->breadcrumbs)):?>
                     <?php $this->widget('zii.widgets.CBreadcrumbs', array(
                         'links'=>$this->breadcrumbs,
                     )); ?>

                 <?php endif?>

         </div>



	<?php echo $content; ?>

	<div class="clear"></div>


</div>

<!--    <div id="footer">-->
<!--        Copyright &copy; --><?php //echo date('Y'); ?><!--<br/>-->
<!--        All Rights Reserved.<br/>-->
<!--        --><?php //echo Yii::powered(); ?>
<!--    </div>-->
    <!-- footer -->


</div><!-- page -->

    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/bootstrap-growl.min.js" >
    </script>
<!--    <input id="y" name="y" value="Всего непрочитанных соощений --><?php //echo Message::model()->getCountUnreaded(Yii::app()->user->id); ?><!--"/>-->
<!--    <script type="text/javascript">-->
<!--        $(document).ready(function(){-->
<!--            $.growl($('#y').val());-->
<!--        });-->
<!--//    </script>-->
</body>

</html>
