<?php

/**
 * This is the model class for table "Article".
 *
 * The followings are the available columns in table 'Article':
 * @property integer $id
 * @property string $name_ru
 * @property string $name_en
 * @property string $shifr
 * @property string $index
 * @property integer $science_id
 * @property string $type_id
 * @property string $date_created
 * @property string $date_updated
 * @property integer $availability
 *
 * The followings are the available model relations:
 * @property DirectScience $science
 * @property Author[] $authors
 * @property File[] $files
 * @property Key[] $keys
 * @property Resume[] $resumes
 */
class Article extends CActiveRecord
{
    const STATUS_CHERNOVIK = 0;
    const STATUS_CONSIDERATION = 1;
    const STATUS_CANCEL = 2;
    const STATUS_PUBLICATION = 3;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'Article';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name_ru, name_en, shifr, index, science_id, type_id, date_created', 'required'),
			array('science_id', 'numerical', 'integerOnly'=>true),
			array('name_ru, name_en, shifr, index, type_id', 'length', 'max'=>64),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name_ru, name_en, shifr, index, science_id, type_id,
			date_created', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'science' => array(self::BELONGS_TO, 'DirectScience', 'science_id'),
			'authors' => array(self::HAS_MANY, 'Author', 'article_id'),
			'files' => array(self::HAS_MANY, 'File', 'article_id'),
			'keys' => array(self::HAS_MANY, 'Key', 'article_id'),
			'resumes' => array(self::HAS_MANY, 'Resume', 'article_id'),
			'pays' => array(self::HAS_MANY, 'Pay', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name_ru' => 'Название публикации на русском языке',
			'name_en' => 'Название публикации на английском языке',
			'shifr' => 'Шифр специальности',
			'index' => 'Индекс УДК публикации',
			'science_id' => 'Научное направление',
			'type_id' => 'Тип публикации',
            'status' => 'Статус',
            'pay' => 'Оплата',
            'date_created' => 'Дата создания',
            'date_updated' => 'Дата изменения',
            'availability' => 'Готовность',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name_ru',$this->name_ru,true);
		$criteria->compare('name_en',$this->name_en,true);
		$criteria->compare('shifr',$this->shifr,true);
		$criteria->compare('index',$this->index,true);
		$criteria->compare('science_id',$this->science_id);
		$criteria->compare('type_id',$this->type_id,true);
        $criteria->compare('date_created',$this->date_created,true);
        $criteria->compare('status_id',$this->status_id,true);
//        $criteria->compare('date_updated',$this->date_updated,true);
//        $criteria->compare('availability',$this->availability);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors() {
        return array(
            'withRelated'=>array(
                'class'=>'common.extensions.wr.WithRelatedBehavior',
            ),
        );
    }

    public function getAvailability(){
        $cnt = 100 / 5 * (
            (count($this)          > 0 ? 1 : 0) +
            (count($this->authors) > 0 ? 1 : 0) +
            (count($this->resumes) > 0 ? 1 : 0) +
            (count($this->keys)    > 0 ? 1 : 0) +
            (count($this->files)   > 0 ? 1 : 0)
        );
        switch($this->status_id){
            case Article::STATUS_CHERNOVIK :
                return (($cnt == 100) && (count($this->pays) > 0)) ?
                    $cnt.'%'.'<br/> <a href="/article/consideration/'.$this->id.'">Отправить публикацию в редакцию</a>' :
                    $cnt.'%' ;
            case Article::STATUS_CONSIDERATION :
                return (($cnt == 100) && (count($this->pays) > 0)) ?
                    $cnt.'%'.'<br/> <a href="/article/publication/'.$this->id.'">Принять к публикации материалы</a>'
                    .'<br/> <a href="/article/cancel/'.$this->id.'">Возвратить автору материалы</a>' :
                    $cnt.'%' ;
        }
    }

    public function getPay(){
        return (count($this->pays) > 0) ?
            'Условно оплачено' :
            'Не оплачено <br> <a href="/pay/'.$this->id.'">Оплатить</a>';
    }

    public function getStatus()
    {
        switch($this->status_id){
            case Article::STATUS_CHERNOVIK : return 'Черновик';
            case Article::STATUS_CONSIDERATION : return 'На рассмотрении';
            case Article::STATUS_PUBLICATION : return 'Принято для публикации';
            case Article::STATUS_CHERNOVIK : return 'Черновик';
            case Article::STATUS_CHERNOVIK : return 'Черновик';

        }
    }

}
