<?php
/**
 * CArrayDataProvider takes an array of AR models for being used in an zii Widget like CListView.
 *
 * CArrayDataProvider may be used in the following way:
 * <pre>
 *
 * $posts = User->posts;
 *
 * $dataProvider=new CArrayDataProvider('Post', $posts);
 * // $dataProvider->getData() will return a list of Post objects belonging to User
 * </pre>
 *
 * @author Herbert Maschke <thyseus@gmail.com>
 * @package system.web
 * @since 1.1
 */
class CArrayDataProvider extends CDataProvider
{
	/**
	 * The Array containing our AR objects
	 */
	public $dataArray;
	/**
	 * @var string the primary ActiveRecord class name. The {@link getData()} method
	 * will return a list of objects of this class.
	 */
	public $modelClass;


	/**
	 * Constructor.
	 * @param string The Dataarray
	 * Any public properties of the data provider can be configured via this parameter
	 */
	public function __construct($modelClass, $dataArray,$config=array())
	{
		if($dataArray === array()) 
			throw new CException(Yii::t('zii','Empty Array given.'));
		$this->dataArray = $dataArray;
		$this->setId($modelClass);
		$this->modelClass = $modelClass;
		foreach($config as $key=>$value)
			$this->$key=$value;
	}

	protected function fetchKeys()
	{
		foreach($this->dataArray[0] as $key=>$value) 
			$keys[] = $key;
		return $keys;
	}

	/**
	 * Since this Dataprovider contains already fetched Objects, we simply return these
	 * @return array
	 */
	protected function fetchData()
	{
		return $this->dataArray;
	}

	/**
	 * Calculates the total number of data items.
	 * @return integer the total number of data items.
	 */
	protected function calculateTotalItemCount()
	{
		return count($this->dataArray);
	}
}
