<?php
/**
 * Specific config overrides for console entry point at local development workstations.
 */
return [
    // Normally you don't have anything to specify for console application at your local.
    'components' => [
        'db' => [
            'connectionString' => 'mysql:host=192.168.1.111;dbname=c3rae',
            'username' => 'c3rae',
            'password' => 'c3rae',
        ]
    ],
];

