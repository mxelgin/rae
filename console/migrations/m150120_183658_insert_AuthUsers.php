<?php

class m150120_183658_insert_AuthUsers extends CDbMigration
{
    public function up()
    {
        $__users__ = array(
            array('id' => '1','username' => 'admin','password' => '827ccb0eea8a706c4c34a16891f84e7b','email' => 'webmaster@example.com','activkey' => '75c634d3c1c20606f4b44908018f5fca','createtime' => '1413132692','lastvisit' => '1414312812','superuser' => '1','status' => '1','create_at' => '0000-00-00 00:00:00','lastvisit_at' => '0000-00-00 00:00:00'),
            array('id' => '2','username' => 'supermoderator','password' => '827ccb0eea8a706c4c34a16891f84e7b','email' => 'supermoderator@test.ru','activkey' => '98ef81dfd7f850ef9508b86293aa7e9d','createtime' => '0','lastvisit' => '1414312626','superuser' => '0','status' => '1','create_at' => '2014-10-13 16:16:47','lastvisit_at' => '0000-00-00 00:00:00'),
        );

        foreach($__users__ as $users){
            $this->insert("AuthUsers",  $users);
        }

    }

    public function down()
    {
        $this->delete("AuthUsers");
        echo "m141026_130815_insert_users does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}