<?php

class m150120_183715_create_AuthProfiles extends CDbMigration
{
    public function up()
    {

        $this->createTable("AuthProfiles",
            array(
                "user_id" => "pk",
                "first_name" => "varchar( 255  )  DEFAULT NULL",
                "last_name" => "varchar( 255  )  DEFAULT NULL",
                "mo_name" => "varchar( 255  ) DEFAULT  ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );
    }

    public function down()
    {
        $this->dropTable("AuthProfiles");
        echo "m141026_133838_create_profiles does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}