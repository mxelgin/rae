<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150214_113801_create_Pay extends CDbMigration
{

	public function up() 
	{
        //Оплата
        $this->createTable("Pay",
            array(
                "id" => "pk",
                "pay" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Оплата'",
                "article_id" => "INT( 11  )  NOT  NULL COMMENT ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        // TODO: Migration actions
        $this->addForeignKey("pay_article_id", "Pay", "article_id","Article","id", "CASCADE ON UPDATE CASCADE");

    }

        public function down()
    {

        // TODO: Migration rollback actions
        $this->dropTable("Pay");
		echo "m150214_113801_create_Pay does not support migration down.\\n";
//		return false;
	}

}

