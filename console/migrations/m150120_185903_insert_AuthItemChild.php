<?php

class m150120_185903_insert_AuthItemChild extends CDbMigration
{
    public function up()
    {
        $AuthItemChild = array(
            array('parent' => 'Moderator','child' => 'Camp.*'),
            array('parent' => 'Moderator','child' => 'Camp.EditAll'),
            array('parent' => 'Moderator','child' => 'Camp.Filter.Doctype'),
            array('parent' => 'Moderator','child' => 'Directory.Filter.Rights'),
            array('parent' => 'Moderator','child' => 'Message.View.*'),
            array('parent' => 'Moderator','child' => 'Quota.Filter.Municipality'),
            array('parent' => 'Moderator','child' => 'Statement.*'),
            array('parent' => 'Moderator','child' => 'Statement.Create'),
            array('parent' => 'Moderator','child' => 'Statement.EditFirst'),
            array('parent' => 'Moderator','child' => 'Statement.Filter.Camp'),
            array('parent' => 'Moderator','child' => 'Statement.Filter.Municipality'),
            array('parent' => 'Moderator','child' => 'Statement.Status.Edit'),
            array('parent' => 'Moderator.Gubkinsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Iset','child' => 'Moderator'),
            array('parent' => 'Moderator.Krasnoselkup','child' => 'Moderator'),
            array('parent' => 'Moderator.Krasnoselkup','child' => 'Camp.Save'),
            array('parent' => 'Moderator.Kurgan','child' => 'Moderator'),
            array('parent' => 'Moderator.Labytnangi','child' => 'Moderator'),
            array('parent' => 'Moderator.Muravlenko','child' => 'Moderator'),
            array('parent' => 'Moderator.Nadymsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Noyabrsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Priuralsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Purovsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Salexard','child' => 'Moderator'),
            array('parent' => 'Moderator.Shuryshkarsk','child' => 'Moderator'),
            array('parent' => 'Moderator.Super','child' => 'Camp.*'),
            array('parent' => 'Moderator.Super','child' => 'Camp.Create'),
            array('parent' => 'Moderator.Super','child' => 'Camp.Delete'),
            array('parent' => 'Moderator.Super','child' => 'Camp.EditAll'),
            array('parent' => 'Moderator.Super','child' => 'Camp.Find'),
            array('parent' => 'Moderator.Super','child' => 'Camp.Save'),
            array('parent' => 'Moderator.Super','child' => 'Camp.SaveAs'),
            array('parent' => 'Moderator.Super','child' => 'Dou.*'),
            array('parent' => 'Moderator.Super','child' => 'Message.View.*'),
            array('parent' => 'Moderator.Super','child' => 'Statement.*'),
            array('parent' => 'Moderator.Super','child' => 'Statement.Create'),
            array('parent' => 'Moderator.Super','child' => 'Statement.EditAll'),
            array('parent' => 'Moderator.Super','child' => 'Statement.Find'),
            array('parent' => 'Moderator.Super','child' => 'Template.*'),
            array('parent' => 'Moderator.Super','child' => 'Template.ViewAll'),
            array('parent' => 'Moderator.Taz','child' => 'Moderator'),
            array('parent' => 'Moderator.Urengoy','child' => 'Moderator'),
            array('parent' => 'Moderator.Yamal','child' => 'Moderator')
        );

        foreach($AuthItemChild as $child){
            $this->insert("AuthItemChild",$child);
        }
    }

    public function down()
    {
        $this->delete("AuthItemChild");
        echo "m141026_114623_insert_AuthItemChild does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}