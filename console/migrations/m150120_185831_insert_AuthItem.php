<?php

class m150120_185831_insert_AuthItem extends CDbMigration
{
    public function up()
    {
        $AuthItem = array(
            array('name' => 'Admin','type' => '2','description' => 'роль Администратор','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'AdminPanel.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.*','type' => '1','description' => 'Шаблоны лагерей','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.Create','type' => '0','description' => 'Лагерь.Создать','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.Delete','type' => '0','description' => 'Лагерь.Удалить','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.EditAll','type' => '0','description' => 'Лагерь.Редактировать все','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.Filter.Doctype','type' => '0','description' => 'Лагерь.Фильтровать по шаблону','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.Find','type' => '0','description' => 'Лагерь.Поиск','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.Save','type' => '0','description' => 'Лагерь.Сохранить','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.SaveAs','type' => '0','description' => 'Лагерь.Сохранить как','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.View.Period','type' => '0','description' => 'Лагерь.Просмотр.Период','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Camp.View.Quota','type' => '0','description' => 'Лагерь.Просмотр.Квота','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'CampImage.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Client.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'ClientCampMO.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'ClientStatement.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectCategory.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectChange.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectCountry.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectDoctype.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectMunicipality.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Directory.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Directory.Filter.Rights','type' => '1','description' => 'Справочники.Фильтровать по правам доступа','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectQueueType.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectQuotaMunicipality.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectSeason.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectStatus.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'DirectYear.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Dou.*','type' => '1','description' => 'Активные ДОУ','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Entry.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Guest','type' => '2','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Message.View.*','type' => '1','description' => 'Сообщения','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator','type' => '2','description' => 'Модератор','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Gubkinsk','type' => '2','description' => 'Модератор Губкинский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Iset','type' => '2','description' => 'Модератор Исетский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Krasnoselkup','type' => '2','description' => 'Модератор Красноселькупский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Kurgan','type' => '2','description' => 'Модератор Курганский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Labytnangi','type' => '2','description' => 'Модератор Лабытнанги','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Muravlenko','type' => '2','description' => 'Модератор Муравленко','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Nadymsk','type' => '2','description' => 'Модератор Надымский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Noyabrsk','type' => '2','description' => 'Модератор Ноябрьск','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Priuralsk','type' => '2','description' => 'Модератор Приуральский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Purovsk','type' => '2','description' => 'Модератор Пуровский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Salexard','type' => '2','description' => 'Модератор Салехард','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Shuryshkarsk','type' => '2','description' => 'Модератор Шурышкарский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Super','type' => '2','description' => 'Супермодератор','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Taz','type' => '2','description' => 'Модератор Тазовский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Urengoy','type' => '2','description' => 'Модератор Новый Уренгой','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Moderator.Yamal','type' => '2','description' => 'Модератор Ямальский','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Queue.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Quota.Filter.Municipality','type' => '0','description' => 'Квота.Фильтровать по муниципальному округу','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Report.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Site.*','type' => '1','description' => NULL,'bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.*','type' => '1','description' => 'Таблицы баз данных','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.Create','type' => '0','description' => 'Заявление.Создать','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.EditAll','type' => '0','description' => 'Заявление.Редактировать все','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.EditFirst','type' => '0','description' => 'Заявление.Редактировать первое','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.Filter.Camp','type' => '0','description' => 'Заявление.Фильтровать по шаблону лагерей','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.Filter.Municipality','type' => '0','description' => 'Заявление.Фильтровать по муниципальному округу','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.Find','type' => '0','description' => 'Заявление.Поиск','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Statement.Status.Edit','type' => '0','description' => 'Заявление.Статус.На редактировании','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Template.*','type' => '1','description' => 'Форма заявлений','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Template.Create','type' => '0','description' => 'Шаблон.Создать','bizrule' => NULL,'data' => 'N;'),
            array('name' => 'Template.ViewAll','type' => '0','description' => 'Шаблон.Просмотреть все','bizrule' => NULL,'data' => 'N;'),
        );

        foreach($AuthItem as $item){
            $this->insert("AuthItem",$item);
        }
    }

    public function down()
    {
        $this->delete("AuthItem");
        echo "m141026_114602_insert_AuthItem does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}