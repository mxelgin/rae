<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150122_172335_create_Article extends CDbMigration
{

	public function up()
	{
        //Публикация publication
        $this->createTable("Article",
            array(
                "id" => "pk",
                "name_ru"            => "varchar( 256  )  NOT  NULL COMMENT 'Название публикации на русском языке'",
                "name_en"            => "varchar( 256  )  NOT  NULL COMMENT 'Название публикации на английском языке'",
                "shifr"              => "varchar( 64  )  NOT  NULL COMMENT 'Шифр специальности'",
                "index"              => "varchar( 64  )  NOT  NULL COMMENT 'Индекс УДК публикации'",
                "science_id"         => "INT( 11  )  NOT  NULL COMMENT 'Научное направление'",
                "type_id"            => "varchar( 64  )  NOT  NULL COMMENT 'Тип публикации'",
                "date_created"       => "timestamp   NOT  NULL default '0000-00-00 00:00:00' COMMENT  'Дата создания'",
                "date_updated"       => "timestamp   NOT  NULL  DEFAULT CURRENT_TIMESTAMP  ON  UPDATE  CURRENT_TIMESTAMP  COMMENT  'Дата изменения'",
                "status_id"          => "INT( 11  )  NOT  NULL DEFAULT 0 COMMENT 'Статус'",
//                "availability"       => "INT ( 11  )  NOT  NULL DEFAULT 20 COMMENT 'Готовность'",
//                "resume_ru" => "varchar( 256  )  NOT  NULL COMMENT 'Резюме на русском языке'",
//                "resume_en" => "varchar( 256  )  NOT  NULL COMMENT 'Резюме на английском языке'",
//                "key_ru" => "varchar( 64  )  NOT  NULL COMMENT 'Ключевые слова на русском языке (через запятую)'",
//                "key_en" => "varchar( 64  )  NOT  NULL COMMENT 'Ключевые слова на английском языке (через запятую)'",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

		// TODO: Migration actions
        $this->addForeignKey("article_science_id", "Article", "science_id","DirectScience","id", "CASCADE ON UPDATE CASCADE");

	}

	public function down()
	{

		// TODO: Migration rollback actions
        $this->dropTable("Article");
		echo "m150122_172335_create_Article does not support migration down.\\n";
//		return false;
	}

}

