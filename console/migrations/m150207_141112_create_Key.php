<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150207_141112_create_Key extends CDbMigration
{

	public function up() 
	{
        //Ключевые слова
        $this->createTable("Key",
            array(
                "id" => "pk",
                "key_ru" => "varchar( 256  )  NOT  NULL COMMENT 'Ключевые слова на русском языке (через запятую)'",
                "key_en" => "varchar( 256  )  NOT  NULL COMMENT 'Ключевые слова на английском языке (через запятую)'",
                "article_id" => "INT( 11  )  NOT  NULL COMMENT ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        // TODO: Migration actions
        $this->addForeignKey("author_key_id", "Key", "article_id","Article","id", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {

        // TODO: Migration rollback actions
        $this->dropTable("Key");
		echo "m150207_141112_create_Key does not support migration down.\\n";
//		return false;
	}

}

