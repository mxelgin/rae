<?php

class m150120_185857_create_AuthItemChild extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthItemChild",
            array(
                "parent" => "varchar( 64  )  NOT  NULL",
                "child" => "varchar( 64  )  NOT  NULL",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        $this->addPrimaryKey("PRIMARY","AuthItemChild","parent, child");
        $this->addForeignKey("parent","AuthItemChild","parent","AuthItem","name", "CASCADE ON UPDATE CASCADE");
        $this->addForeignKey("child","AuthItemChild","child","AuthItem","name", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {
        $this->dropTable("AuthItemChild");
        echo "m141026_114617_create_AuthItemChild does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}