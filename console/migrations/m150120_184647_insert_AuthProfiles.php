<?php

class m150120_184647_insert_AuthProfiles extends CDbMigration
{
    public function up()
    {
        $__profiles__ = array(
            array('user_id' => '1','first_name' => 'Administrator','last_name' => 'Admin','mo_name' => ''),
            array('user_id' => '2','first_name' => 'Модератор','last_name' => '','mo_name' => ''),
        );

        foreach($__profiles__ as $profiles){
            $this->insert("AuthProfiles",  $profiles);
        }

    }

    public function down()
    {
        $this->delete("AuthProfiles");
        echo "m141026_130836_insert_profiles does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}