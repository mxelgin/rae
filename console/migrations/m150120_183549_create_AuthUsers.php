<?php

class m150120_183549_create_AuthUsers extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthUsers",
            array(
                "id" => "pk",
                "username" => "varchar( 20  )  NOT  NULL DEFAULT  ''",
                "password" => "varchar( 128  )  NOT  NULL DEFAULT  ''",
                "email" => "varchar( 128  )  NOT  NULL DEFAULT  ''",
                "activkey" => "varchar( 128  )  NOT  NULL DEFAULT  ''",
                "createtime" => "int( 10  )  NOT  NULL DEFAULT  '0'",
                "lastvisit" => "int( 10  )  NOT  NULL DEFAULT  '0'",
                "superuser" => "int( 1  )  NOT  NULL DEFAULT  '0'",
                "status" => "int( 1  )  NOT  NULL DEFAULT  '0'",
                "create_at" => "timestamp NOT  NULL  DEFAULT CURRENT_TIMESTAMP",
                "lastvisit_at" => "timestamp NOT  NULL DEFAULT  '0000-00-00 00:00:00'",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );
        $this->createIndex("user_username","AuthUsers","username",true);
        $this->createIndex("user_email","AuthUsers","email",true);
    }

    public function down()
    {
        $this->dropTable("AuthUsers");
        echo "m141026_130807_create_users does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}