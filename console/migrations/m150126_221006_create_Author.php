<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150126_221006_create_Author extends CDbMigration
{

	public function up() 
	{
        $this->createTable("Author",
            array(
                "id" => "pk",
                "family_ru" => "varchar( 64  )  NOT  NULL COMMENT 'Фамилия автора на русском языке'",
                "name_ru" => "varchar( 64  )  NOT  NULL COMMENT 'Имя автора на русском языке'",
                "lastname_ru" => "varchar( 64  )  NOT  NULL COMMENT 'Отчество автора на русском языке'",
                "email" => "varchar( 64  )  NOT  NULL COMMENT 'Адрес электронной почты автора'",
                "work_ru" => "varchar( 256  )  NOT  NULL COMMENT 'Место работы автора на русском языке'",
                "work_en" => "varchar( 256  )  NOT  NULL COMMENT 'Место работы автора на английском языке'",
                "position_ru" => "varchar( 64  )  NOT  NULL COMMENT 'Должность автора на русском языке'",
                "article_id" => "INT( 11  )  NOT  NULL COMMENT ''",

            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        // TODO: Migration actions
        $this->addForeignKey("author_article_id", "Author", "article_id","Article","id", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {

        // TODO: Migration rollback actions
        $this->dropTable("Author");

		echo "m150126_221006_create_Author does not support migration down.\\n";
//		return false;
	}

}

