<?php

class m150120_184703_create_AuthRights extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthRights",
            array(
                "itemname" => "varchar( 64  )  NOT  NULL",
                "type" => "int( 11  )  NOT  NULL",
                "weight" => "int( 11  )  NOT  NULL",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  'Права'" );
        $this->addPrimaryKey("itemname","AuthRights","itemname");
//        $this->addForeignKey("itemname","Rights","itemname","AuthItem","name", "CASCADE ON UPDATE CASCADE");
    }

    public function down()
    {
        $this->dropTable("AuthRights");
        echo "m150120_184703_create_AuthRights does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}