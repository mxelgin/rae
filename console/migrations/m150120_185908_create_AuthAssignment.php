<?php

class m150120_185908_create_AuthAssignment extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthAssignment",
            array(
                "itemname" => "varchar( 64  )  NOT  NULL",
                "userid" => "varchar( 64  )  NOT  NULL",
                "bizrule" => "text",
                "data" => "text",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        $this->addPrimaryKey("itemname","AuthAssignment","itemname");
        $this->addForeignKey("itemname","AuthAssignment","itemname","AuthItem","name", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {
        $this->dropTable("AuthAssignment");
        echo "m141026_114638_create_AuthAssignment does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}