<?php

class m150120_184750_create_AuthProfilesFields extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthProfilesFields",
            array(
                "id" => "pk",
                "varname" => "varchar( 50  )  NOT  NULL DEFAULT  ''",
                "title" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "field_type" => "varchar( 50  )  NOT  NULL DEFAULT  ''",
                "field_size" => "int( 3  )  NOT  NULL DEFAULT  '0'",
                "field_size_min" => "int( 3  )  NOT  NULL DEFAULT  '0'",
                "required" => "int( 1  )  NOT  NULL DEFAULT  '0'",
                "match" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "range" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "error_message" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "other_validator" => "text",
                "default" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "widget" => "varchar( 255  )  NOT  NULL DEFAULT  ''",
                "widgetparams" => "text",
                "position" => "int( 3  )  NOT  NULL DEFAULT  '0'",
                "visible" => "int( 1  )  NOT  NULL DEFAULT  '0'",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

//        $this->addPrimaryKey("PRIMARY","{{profiles_fields}}","id");
    }

    public function down()
    {
        $this->dropTable("AuthProfilesFields");
        echo "m141026_130848_create_profiles_fields does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}