<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150207_141044_create_Resume extends CDbMigration
{

	public function up() 
	{
        //Резюме
        $this->createTable("Resume",
            array(
                "id" => "pk",
                "resume_ru" => "varchar( 256  )  NOT  NULL COMMENT 'Резюме на русском языке'",
                "resume_en" => "varchar( 256  )  NOT  NULL COMMENT 'Резюме на английском языке'",
                "article_id" => "INT( 11  )  NOT  NULL COMMENT ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        // TODO: Migration actions
        $this->addForeignKey("author_resume_id", "Resume", "article_id","Article","id", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {

        // TODO: Migration rollback actions
        $this->dropTable("Resume");
		echo "m150207_141044_create_Resume does not support migration down.\\n";
//		return false;
	}

}

