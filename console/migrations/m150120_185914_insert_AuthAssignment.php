<?php

class m150120_185914_insert_AuthAssignment extends CDbMigration
{
    public function up()
    {
        $AuthAssignment = array(
            array('itemname' => 'Admin','userid' => '1','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Gubkinsk','userid' => '9','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Iset','userid' => '17','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Krasnoselkup','userid' => '16','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Kurgan','userid' => '18','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Labytnangi','userid' => '6','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Muravlenko','userid' => '4','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Nadymsk','userid' => '10','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Noyabrsk','userid' => '8','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Priuralsk','userid' => '12','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Purovsk','userid' => '15','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Salexard','userid' => '5','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Shuryshkarsk','userid' => '11','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Super','userid' => '3','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Taz','userid' => '14','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Urengoy','userid' => '7','bizrule' => NULL,'data' => 'N;'),
            array('itemname' => 'Moderator.Yamal','userid' => '13','bizrule' => NULL,'data' => 'N;')
        );

        foreach($AuthAssignment as $Assignment){
            $this->insert("AuthAssignment",$Assignment);
        }
    }

    public function down()
    {
        $this->delete("AuthAssignment");
        echo "m150120_185914_insert_AuthAssignment does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}