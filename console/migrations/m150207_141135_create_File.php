<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150207_141135_create_File extends CDbMigration
{

	public function up() 
	{
        //Файл
        $this->createTable("File",
            array(
                "id" => "pk",
                "file1" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Текст статьи с рисунками и таблицами'",
                "file2" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Сканированная сторонняя рецензия №1 (доктора наук)'",
                "file3" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Сканированная сторонняя рецензия №2 (доктора наук)'",
                "file4" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Сканированная сопроводительное письмо (подписанное руководителем учреждения или первым автором)'",
                "file5" => "VARCHAR( 255  )  NOT  NULL  COMMENT  'Сканированное экспертное заключение (о возможности публикации материалов в открытой печати)'",
                "article_id" => "INT( 11  )  NOT  NULL COMMENT ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        // TODO: Migration actions
        $this->addForeignKey("author_file_id", "File", "article_id","Article","id", "CASCADE ON UPDATE CASCADE");

    }

    public function down()
    {

        // TODO: Migration rollback actions
        $this->dropTable("File");
		echo "m150207_141135_create_File does not support migration down.\\n";
//		return false;
	}

}

