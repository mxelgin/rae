<?php

class m150120_183028_create_messages extends CDbMigration
{
    public function up()
    {
        $this->createTable("Messages",
            array(
                "id" => "pk",
                "sender_id" => "int(11) NOT NULL",
                "receiver_id" => "int(11) NOT NULL",
                "subject" => "varchar(256) NOT NULL DEFAULT ''",
                "body" => "text",
                "is_read" => "enum('0','1') NOT NULL DEFAULT '0'",
                "deleted_by" => "enum('sender','receiver') DEFAULT NULL",
                "created_at" => "datetime NOT NULL",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        $this->createIndex("sender_id", "Messages", "sender_id");
        $this->createIndex("receiver_id", "Messages", "receiver_id");

    }

    public function down()
    {
        $this->dropTable("Messages");
        echo "m141031_073356_create_messages does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}