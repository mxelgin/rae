<?php

class m150120_185816_create_AuthItem extends CDbMigration
{
    public function up()
    {
        $this->createTable("AuthItem",
            array(
                "name" => "varchar( 64  )  NOT  NULL",
                "type" => "int( 11  )  NOT  NULL",
                "description" => "text",
                "bizrule" => "text",
                "data" => "text",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  ''" );

        $this->addPrimaryKey("PRIMARY","AuthItem","name");
    }

    public function down()
    {
        $this->dropTable("AuthItem");
        echo "m150120_185816_create_AuthItem does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}