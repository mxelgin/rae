<?php

class m150120_184758_insert_AuthProfilesFields extends CDbMigration
{
    public function up()
    {
        $__profiles_fields__ = array(
            array('id' => '1','varname' => 'first_name','title' => 'First Name','field_type' => 'VARCHAR','field_size' => '255','field_size_min' => '3','required' => '2','match' => '','range' => '','error_message' => 'Incorrect First Name (length between 3 and 50 characters).','other_validator' => '','default' => '','widget' => '','widgetparams' => '','position' => '1','visible' => '2'),
            array('id' => '2','varname' => 'last_name','title' => 'Last Name','field_type' => 'VARCHAR','field_size' => '255','field_size_min' => '3','required' => '2','match' => '','range' => '','error_message' => 'Incorrect Last Name (length between 3 and 50 characters).','other_validator' => '','default' => '','widget' => '','widgetparams' => '','position' => '2','visible' => '2'),
//            array('id' => '3','varname' => 'mo_name','title' => 'Муниципальный Округ','field_type' => 'VARCHAR','field_size' => '255','field_size_min' => '3','required' => '2','match' => '','range' => '','error_message' => '','other_validator' => '','default' => '','widget' => '','widgetparams' => '','position' => '3','visible' => '3')
        );

        foreach($__profiles_fields__ as $fields){
            $this->insert("AuthProfilesFields",  $fields);
        }

    }

    public function down()
    {
        $this->delete("AuthProfilesFields");
        echo "m141026_130855_insert_profiles_fields does not support migration down.\n";
//		return false;
    }

    /*
    // Use safeUp/safeDown to do migration with transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}