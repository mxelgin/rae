<?php
/**
 * TODO: Migration description
 *
 * @package migrations
 */
class m150121_084728_create_DirectType extends CDbMigration
{

	public function up() 
	{
        $this->createTable("DirectType",
            array(
                "id" => "pk",
                "name" => "varchar( 64  )  NOT  NULL COMMENT ''",
            ), "ENGINE = INNODB DEFAULT CHARSET = utf8 COMMENT =  'Тип публикации'" );

		// TODO: Migration actions

	}

	public function down() 
	{

		// TODO: Migration rollback actions
        $this->dropTable("DirectType");

		echo "m150121_084728_create_DirectType does not support migration down.\\n";
//		return false;
	}

}

