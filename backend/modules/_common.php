<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 11.11.14
 * Time: 13:25
 */

$btnTemplates = array(
    array('label'=>'Таблица баз данных', 'url' => '/configStatement','icon'=>' icon-th-large','authItemName'=>'ConfigStatement.Admin',),
    array('label'=>'Муниципальный округ', 'url' => '/configDirectMo','icon'=>' icon-th-large','authItemName'=>'ConfigStatement.Admin',),
    array('label'=>'Шаблон', 'url' => '/configDirectDoctype','icon'=>' icon-th-large','authItemName'=>'ConfigStatement.Admin',),
);

$this->menu=array(
    array(
        'class'=>'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label' => 'Справочники',
                'type'=>'success',
                'items' =>$btnTemplates
            ),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label' => UserModule::t('Users'),
                'type'=>'success',
                'items' =>array(

//                    array('label'=>UserModule::t('Create User'), 'url'=>array('create')),
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
                    array('label'=>UserModule::t('Manage Profile Field'), 'url'=>array('profileField/admin')),
                    array('label'=>UserModule::t('List User'), 'url'=>array('/user')),
                ))
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label' => UserModule::t('Rights'),
                'type'=>'success',
                'items' =>array(
                    array(
                        'label'=>Rights::t('core', 'Assignments'),
                        'type'=>'success',
                        'url'=>array('/rights/assignment/view'),
                        'itemOptions'=>array('class'=>'item-assignments'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Permissions'),
                        'type'=>'success',
                        'url'=>array('/rights/authItem/permissions'),
                        'itemOptions'=>array('class'=>'item-permissions'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Roles'),
                        'type'=>'success',
                        'url'=>array('/rights/authItem/roles'),
                        'itemOptions'=>array('class'=>'item-roles'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Tasks'),
                        'type'=>'success',
                        'url'=>array('/rights/authItem/tasks'),
                        'itemOptions'=>array('class'=>'item-tasks'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Operations'),
                        'type'=>'success',
                        'url'=>array('/rights/authItem/operations'),
                        'itemOptions'=>array('class'=>'item-operations'),
                    ),
//                    array(
//                        'label'=>'Права',
//                        'type'=>'primary',
//                        'url'=>array('/rights'),
//                        'itemOptions'=>array('class'=>'item-operations'),
//                    ),
                )
            )
        ),
    ),
);

?>