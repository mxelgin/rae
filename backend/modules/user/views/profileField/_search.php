<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>


    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr class="odd">
            <th>
                Id
            </th>
            <td>
                <?php echo $form->textField($model,'id',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Имя переменной
            </th>
            <td>
                <?php echo $form->textField($model,'varname',array('size'=>50,'maxlength'=>50, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Название
            </th>
            <td>
                <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Тип поля
            </th>
            <td>
                <?php echo $form->dropDownList($model,'field_type',ProfileField::itemAlias('field_type')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Размер поля
            </th>
            <td>
                <?php echo $form->textField($model,'field_size',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Минимальное значение
            </th>
            <td>
                <?php echo $form->textField($model,'field_size_min',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Обязательность
            </th>
            <td>
                <?php echo $form->dropDownList($model,'required',ProfileField::itemAlias('required')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Совпадение (RegExp)
            </th>
            <td>
                <?php echo $form->textField($model,'match',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Ряд значений
            </th>
            <td>
                <?php echo $form->textField($model,'range',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Сообщение об ошибке
            </th>
            <td>
                <?php echo $form->textField($model,'error_message',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Другой валидатор
            </th>
            <td>
                <?php echo $form->textField($model,'other_validator',array('size'=>60,'maxlength'=>5000, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                По умолчанию
            </th>
            <td>
                <?php echo $form->textField($model,'default',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Виджет
            </th>
            <td>
                <?php echo $form->textField($model,'widget',array('size'=>60,'maxlength'=>255, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Параметры виджета
            </th>
            <td>
                <?php echo $form->textField($model,'widgetparams',array('size'=>60,'maxlength'=>5000, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Позиция
            </th>
            <td>
                <?php echo $form->textField($model,'position',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Видимость
            </th>
            <td>
                <?php echo $form->dropDownList($model,'visible',ProfileField::itemAlias('visible')); ?>
            </td>
        </tr>
        </tbody>
    </table>


    <div class="form-actions">
        <div class="pull-right">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'success',
                'label'=>'Поиск',
            )); ?>
        </div>
    </div>



 <!--    <div class="row buttons">-->
<!--        --><?php //echo CHtml::submitButton(UserModule::t('Search')); ?>
<!--    </div>-->

<?php $this->endWidget(); ?>

</div><!-- search-form --> 