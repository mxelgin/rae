<?php
$this->breadcrumbs=array(
//	UserModule::t('Profile Fields')=>array('admin'),
//	$model->title=>array('view','id'=>$model->id),
//	UserModule::t('Update'),
    'Управление пользователями'=>array('/user'),
    'Настройка полей'=>array('/user/profileField'),
    'Изменить'
);

$this->titleBox = '<h3>Изменить</h3>';

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
            array(
                'label'=>UserModule::t('Save'),
                'type'=>'success',
                'buttonType'=>'submit',
//                'url'=>array('/user/profileField'),
                'htmlOptions'=>array(
                    'onclick' => '$("#user-fields-form").submit()',
                )
            ),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Create Profile Field'),'type'=>'primary', 'url'=>array('create')),
//            array('label'=>UserModule::t('View Profile Field'), 'type'=>'primary', 'url'=>array('view','id'=>$model->id)),
            array('label'=>UserModule::t('Manage Profile Field'), 'type'=>'success', 'url'=>array('admin')),
            array('label'=>UserModule::t('List User'), 'type'=>'success','url'=>array('/user')),
        ),
    ),
);
?>

<!--<h1>--><?php //echo UserModule::t('Update Profile Field ').$model->id; ?><!--</h1>-->
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>