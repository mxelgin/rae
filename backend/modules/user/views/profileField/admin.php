<?php
$this->breadcrumbs=array(
//	UserModule::t('Profile Fields')=>array('admin'),
//	UserModule::t('Manage'),
    'Управление пользователями'=>array('/user'),
    'Настройка полей'
);

$this->renderPartial('/../../_common');

$_search = array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label'=>'Расширенный поиск',
                'type'=>'success',
                'htmlOptions' => array(
                    'id'=>'menu_searchBtn',
                    'onclick' => '$(".search-form").toggle()',
                )),
        ),
    ),
);

$_field_new = array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array( array('label'=>UserModule::t('Create Profile Field'),'type'=>'success', 'url'=>array('create')) ),
    ),
);

$this->menu = array_merge($this->menu, $_search);
$this->menu = array_merge($this->menu, $_field_new);

//$this->menu=array(
//    array(
//        'class' => 'bootstrap.widgets.TbButtonGroup',
//        'buttons' => array(
//            array('label'=>UserModule::t('Create Profile Field'),'type'=>'primary', 'url'=>array('create')),
////            array('label'=>UserModule::t('Manage Profile Field'),'type'=>'primary', 'url'=>array('admin')),
////            array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
////            array('label'=>UserModule::t('List User'),'type'=>'primary', 'url'=>array('/user')),
//        ),
//    ),
//    array(
//        'class' => 'bootstrap.widgets.TbButtonGroup',
//        'buttons' => array(
////            array('label'=>UserModule::t('Create Profile Field'),'type'=>'primary', 'url'=>array('create')),
//            array('label'=>UserModule::t('Manage Profile Field'),'type'=>'primary', 'url'=>array('admin')),
////            array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
//            array('label'=>UserModule::t('List User'),'type'=>'primary', 'url'=>array('/user')),
//        ),
//    ),
//    array(
//        'class' => 'bootstrap.widgets.TbButtonGroup',
//        'buttons' => array(
//            array(
//                'label'=>'Расширенный поиск',
//                'type'=>'primary',
//                'htmlOptions' => array(
//                    'id'=>'menu_searchBtn',
//                    'onclick' => '$(".search-form").toggle()',
//                )),
//        ),
//    ),
//
//);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('profile-field-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>

<?php $this->titleBox = '<h3>Настройка полей</h3>'; ?>
<!--<h1>--><?php //echo UserModule::t('Manage Profile Fields'); ?><!--</h1>-->

<div class="form-content">
<?php echo CHtml::link(UserModule::t('Advanced Search'),'#',array('class'=>'search-button hidden')); ?>
<div class="search-form" style="display:none">
<p><?php echo UserModule::t("You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done."); ?></p>
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'dataProvider'=>$model->search(),
	'filter'=>$model,
    'type'=>'striped bordered',
    'htmlOptions'=>array('class'=>'grid-view table_custom'),
	'columns'=>array(
		'id',
		array(
			'name'=>'varname',
			'type'=>'raw',
			'value'=>'UHtml::markSearch($data,"varname")',
		),
		array(
			'name'=>'title',
			'value'=>'UserModule::t($data->title)',
		),
		array(
			'name'=>'field_type',
			'value'=>'$data->field_type',
			'filter'=>ProfileField::itemAlias("field_type"),
		),
		'field_size',
		//'field_size_min',
		array(
			'name'=>'required',
			'value'=>'ProfileField::itemAlias("required",$data->required)',
			'filter'=>ProfileField::itemAlias("required"),
		),
		//'match',
		//'range',
		//'error_message',
		//'other_validator',
		//'default',
		'position',
		array(
			'name'=>'visible',
			'value'=>'ProfileField::itemAlias("visible",$data->visible)',
			'filter'=>ProfileField::itemAlias("visible"),
		),
		//*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
</div>