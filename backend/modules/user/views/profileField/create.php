<?php
$this->breadcrumbs=array(
//	UserModule::t('Profile Fields')=>array('admin'),
//	UserModule::t('Create'),
    'Управление пользователями'=>array('/user'),
    'Настройка полей'=>array('/user/profileField'),
    'Добавить'
);

$this->titleBox = '<h3>Добавить</h3>';

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
            array(
                'label'=>UserModule::t('Create'),
                'type'=>'success',
                'buttonType'=>'submit',
//                'url'=>array('/user/profileField'),
                'htmlOptions'=>array(
                    'onclick' => '$("#user-fields-form").submit()',
                )
            ),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>UserModule::t('Manage Profile Field'),'type'=>'success', 'url'=>array('admin')),
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('/user/admin')),
            array('label'=>UserModule::t('List User'),'type'=>'success', 'url'=>array('/user')),
        ),
    ),
);
?>
<!--<h1>--><?php //echo UserModule::t('Create Profile Field'); ?><!--</h1>-->
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>