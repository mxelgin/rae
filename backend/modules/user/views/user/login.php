<?php
$this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Login");
//$this->breadcrumbs=array(
//	UserModule::t("Login"),
//);
?>

<?php if(Yii::app()->user->hasFlash('loginMessage')): ?>

    <div class="success">
        <?php echo Yii::app()->user->getFlash('loginMessage'); ?>
    </div>

<?php endif; ?>

<!--    <div class="form form-signin">-->

<!--            <legend>Legend</legend>-->

        <?php
        $form = $this->beginWidget(
            'bootstrap.widgets.TbActiveForm',
            array(
                'id' => 'verticalForm',
                'htmlOptions'=>array('class'=>'form form-signin')
//                'title'=>'olo'
            )
        );
        ?>
            <legend class="text-center">Форма авторизации</legend>

        <?php echo CHtml::errorSummary($model); ?>


        <div class="input-prepend ">
            <span class="add-on ">
                <i class="icon-user"></i>
            </span>
            <!--        --><?php //echo CHtml::activeLabelEx($model,'username'); ?>
            <?php echo CHtml::activeTextField($model,'username', array('class'=>'span12 login-input form-inside text-green', 'placeholder'=>'Логин' )) ?>
        </div>

        <div class="input-prepend ">
            <span class="add-on ">
                <i class="icon-lock"></i>
            </span>
            <?php //echo CHtml::activeLabelEx($model,'password'); ?>
            <?php echo CHtml::activePasswordField($model,'password', array('class'=>'span12 login-input form-inside text-green', 'placeholder'=>'Пароль' )) ?>
        </div>

        <?php echo CHtml::submitButton(UserModule::t("Login"), array('class'=>'span5 login-input form-inside btn-login btn btn-success')); ?>
        <p></p>
        <div class="row text-center login-input">
            <p class="hint text-gray">
                <?php echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?>
<!--                | --><?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
            </p>
        </div>

        <?php $this->endWidget(); ?>
<!--    </div>-->

<?php
$form = new CForm(array(
    'elements'=>array(
        'username'=>array(
            'type'=>'text',
            'maxlength'=>32,
        ),
        'password'=>array(
            'type'=>'password',
            'maxlength'=>32,
        ),
        'rememberMe'=>array(
            'type'=>'checkbox',
        )
    ),

    'buttons'=>array(
        'login'=>array(
            'type'=>'submit',
            'label'=>'Login',
        ),
    ),
), $model);
?>

<!--<div class="row">-->
<!--    --><?php //echo CHtml::activeLabelEx($model,'username'); ?>
<!--    --><?php //echo CHtml::activeTextField($model,'username') ?>
<!--</div>-->
<!---->
<!--<div class="row">-->
<!--    --><?php //echo CHtml::activeLabelEx($model,'password'); ?>
<!--    --><?php //echo CHtml::activePasswordField($model,'password') ?>
<!--</div>-->
<!---->
<!--<div class="row">-->
<!--    <p class="hint">-->
<!--        --><?php //echo CHtml::link(UserModule::t("Register"),Yii::app()->getModule('user')->registrationUrl); ?><!-- | --><?php //echo CHtml::link(UserModule::t("Lost Password?"),Yii::app()->getModule('user')->recoveryUrl); ?>
<!--    </p>-->
<!--</div>-->
<!---->
<!--<div class="row rememberMe">-->
<!--    --><?php //echo CHtml::activeCheckBox($model,'rememberMe'); ?>
<!--    --><?php //echo CHtml::activeLabelEx($model,'rememberMe'); ?>
<!--</div>-->
<!---->
<!--<div class="row submit">-->
<!--    --><?php //echo CHtml::submitButton(UserModule::t("Login")); ?>
<!--</div>-->
