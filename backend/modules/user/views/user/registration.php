<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Registration");
$this->breadcrumbs=array(
	UserModule::t("Registration"),
);
$this->titleBox = '<h3>Регистрация</h3>';
?>

<!--<h1>--><?php //echo UserModule::t("Registration"); ?><!--</h1>-->

<?php if(Yii::app()->user->hasFlash('registration')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('registration'); ?>
</div>
<?php else: ?>

<div class="form form-content">
<?php $form=$this->beginWidget('UActiveForm', array(
	'id'=>'registration-form',
	'enableAjaxValidation'=>true,
	'disableAjaxValidationAttributes'=>array('RegistrationForm_verifyCode'),
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
	),
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
)); ?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>




    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr>
            <th><?php echo $form->labelEx($model,'username'); ?></th>
            <td>
                <?php echo $form->textField($model,'username'); ?>
            </td>
            <td>
                <?php echo $form->error($model,'username'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model,'password'); ?></th>
            <td>
                <?php echo $form->passwordField($model,'password'); ?>
            </td>
            <td>
                <?php echo $form->error($model,'password'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model,'verifyPassword'); ?></th>
            <td>
                <?php echo $form->passwordField($model,'verifyPassword'); ?>
            </td>
            <td>
                <?php echo $form->error($model,'verifyPassword'); ?>
            </td>
        </tr>
        <tr>
            <th><?php echo $form->labelEx($model,'email'); ?></th>
            <td>
                <?php echo $form->textField($model,'email'); ?>
            </td>
            <td>
                <?php echo $form->error($model,'email'); ?>
            </td>
        </tr>














<!--	<div class="row">-->
<!--	--><?php //echo $form->labelEx($model,'username'); ?>
<!--	--><?php //echo $form->textField($model,'username'); ?>
<!--	--><?php //echo $form->error($model,'username'); ?>
<!--	</div>-->
<!--	-->
<!--	<div class="row">-->
<!--	--><?php //echo $form->labelEx($model,'password'); ?>
<!--	--><?php //echo $form->passwordField($model,'password'); ?>
<!--	--><?php //echo $form->error($model,'password'); ?>
<!--	<p class="hint">-->
<!--	--><?php //echo UserModule::t("Minimal password length 4 symbols."); ?>
<!--	</p>-->
<!--	</div>-->
<!--	-->
<!--	<div class="row">-->
<!--	--><?php //echo $form->labelEx($model,'verifyPassword'); ?>
<!--	--><?php //echo $form->passwordField($model,'verifyPassword'); ?>
<!--	--><?php //echo $form->error($model,'verifyPassword'); ?>
<!--	</div>-->
<!--	-->
<!--	<div class="row">-->
<!--	--><?php //echo $form->labelEx($model,'email'); ?>
<!--	--><?php //echo $form->textField($model,'email'); ?>
<!--	--><?php //echo $form->error($model,'email'); ?>
<!--	</div>-->

        <?php
                $profileFields=$profile->getFields();
                if ($profileFields) {
                    foreach($profileFields as $field) {
                    ?>
            <tr>
                <th>
                    <?php echo $form->labelEx($profile,$field->varname); ?>
                </th>
                <td>
                <?php
                if ($widgetEdit = $field->widgetEdit($profile)) {
                    echo $widgetEdit;
                } elseif ($field->range) {
                    echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
                } elseif ($field->field_type=="TEXT") {
                    echo$form->textArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
                } else {
                    echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255)));
                }
                 ?>
                </td>
                <td>
                    <?php echo $form->error($profile,$field->varname); ?>
                </td>
            </tr>
                    <?php
                    }
                }
        ?>

        <th>
            <?php echo $form->labelEx($model,'verifyCode'); ?>
        </th>
        <td>
            <?php echo $form->textField($model,'verifyCode'); ?>
        </td>
        <td>
            <?php echo $form->error($model,'verifyCode'); ?>
        </td>
        </tbody>
    </table>


	<?php if (UserModule::doCaptcha('registration')): ?>

<!--        <table class="detail-view table table-striped table-condensed">-->
<!--            <tbody>-->
<!--            <th>-->
<!--                --><?php //echo $form->labelEx($model,'verifyCode'); ?>
<!--            </th>-->
<!--            <td>-->
<!--                --><?php //echo $form->textField($model,'verifyCode'); ?>
<!--            </td> -->
<!--            </tbody>-->
<!--            </table>-->

		<?php $this->widget('CCaptcha'); ?>



		<p class="hint"><?php echo UserModule::t("Please enter the letters as they are shown in the image above."); ?>
		<br/><?php echo UserModule::t("Letters are not case-sensitive."); ?></p>

	<?php endif; ?>

<!--	<div class="row submit">-->
<!--		--><?php //echo CHtml::submitButton(UserModule::t("Register")); ?>
<!--	</div>-->
</div><!-- form -->

    <div class="modal-footer">
        <?php echo CHtml::submitButton(UserModule::t("Register"), array('class'=>'btn btn-success')); ?>
    </div>
<?php $this->endWidget(); ?>

<?php endif; ?>