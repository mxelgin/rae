<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>


    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr class="odd">
            <th>
<!--                --><?php //echo $form->label($model,'id'); ?>
                Id
            </th>
            <td>
                <?php echo $form->textField($model,'id',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Логин
<!--                --><?php //echo $form->label($model,'username'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Электронная почта
<!--                --><?php //echo $form->label($model,'email'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Ключ активации
<!--                --><?php //echo $form->label($model,'activkey'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'activkey',array('size'=>60,'maxlength'=>128, 'class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Дата регистрации
<!--                --><?php //echo $form->label($model,'create_at'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'create_at',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Последний визит
<!--                --><?php //echo $form->label($model,'lastvisit_at'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'lastvisit_at',array('class'=>'span12')); ?>
            </td>
        </tr>
        <tr class="odd">
            <th>
                Супер пользователь
<!--                --><?php //echo $form->label($model,'superuser'); ?>
            </th>
            <td>
                <?php echo $form->dropDownList($model,'superuser',$model->itemAlias('AdminStatus')); ?>
            </td>
        </tr>
        <tr class="even">
            <th>
                Статус
<!--                --><?php //echo $form->label($model,'status'); ?>
            </th>
            <td>
                <?php echo $form->dropDownList($model,'status',$model->itemAlias('UserStatus')); ?>
            </td>
        </tr>
        </tbody>
    </table>

<!--    <div class="row buttons">-->
<!--        --><?php //echo CHtml::submitButton(UserModule::t('Search')); ?>
<!--    </div>-->
    <div class="form-actions">
        <div class="pull-right">
            <?php $this->widget('bootstrap.widgets.TbButton', array(
                'buttonType'=>'submit',
                'type'=>'success',
                'label'=>'Поиск',
            )); ?>
        </div>
    </div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->