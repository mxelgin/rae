<?php
$this->breadcrumbs=array(
//	UserModule::t('Users')=>array('/user'),
//	UserModule::t('Manage'),
    'Управление пользователями'
);

$this->renderPartial('/../../_common');

$_search = array(    array(
    'class' => 'bootstrap.widgets.TbButtonGroup',
    'buttons' => array(
        array(
            'label'=>'Расширенный поиск',
            'type'=>'success',
            'htmlOptions' => array(
                'id'=>'menu_searchBtn',
                'onclick' => '$(".search-form").toggle()',
            )),
    ),
),
);

$_user_new = array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label'=>UserModule::t('Create User'),
                'type'=>'success',
                'url'=>array('create'),
            ),
        ),
    ),
);

$this->menu = array_merge($this->menu, $_search);
$this->menu = array_merge($this->menu, $_user_new);



Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
    $.fn.yiiGridView.update('user-grid', {
        data: $(this).serialize()
    });
    return false;
});
");

?>

<?php $this->titleBox = '<h3>Управление пользователями</h3>'; ?>
<!--<h1>--><?php //echo UserModule::t("Manage Users"); ?><!--</h1>-->

<!--<p>--><?php //echo UserModule::t("You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b> or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done."); ?><!--</p>-->

<div class="form-content">
<?php echo CHtml::link(UserModule::t('Advanced Search'),'#',array('class'=>'search-button hidden')); ?>
<div class="search-form" style="display:none">
    <p>
        Вы можете использовать операторы (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
        or <b>=</b>) для поиска нужных позиций.
    </p><br>
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'user-grid',
    'type'=>'striped bordered',
	'dataProvider'=>$model->search(),
    'htmlOptions'=>array('class'=>'grid-view table_custom'),
	'filter'=>$model,
	'columns'=>array(
		array(
			'name' => 'id',
			'type'=>'raw',
			'value' => 'CHtml::link(CHtml::encode($data->id),array("admin/update","id"=>$data->id))',
		),
		array(
			'name' => 'username',
			'type'=>'raw',
			'value' => 'CHtml::link(UHtml::markSearch($data,"username"),array("admin/view","id"=>$data->id))',
		),
		array(
			'name'=>'email',
			'type'=>'raw',
			'value'=>'CHtml::link(UHtml::markSearch($data,"email"), "mailto:".$data->email)',
		),
		'create_at',
		'lastvisit_at',
		array(
			'name'=>'superuser',
			'value'=>'User::itemAlias("AdminStatus",$data->superuser)',
			'filter'=>User::itemAlias("AdminStatus"),
		),
		array(
			'name'=>'status',
			'value'=>'User::itemAlias("UserStatus",$data->status)',
			'filter' => User::itemAlias("UserStatus"),
		),
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
            'template'=>'{update}{delete}',
		),
	),
)); ?>
</div>