<?php
$this->breadcrumbs=array(
//	UserModule::t('Users')=>array('admin'),
//	UserModule::t('Create'),
    'Управление пользователями'=>array('/user'),
    'Добавить'
);

$this->titleBox = '<h3>Добавить</h3>';


$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
            array(
                'label'=>UserModule::t('Create'),
                'type'=>'success',
                'buttonType'=>'submit',
                'url'=>array('/user'),
                'htmlOptions'=>array(
                    'onclick' => '$("#user-form").submit()',
                )
            ),
         ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
//            array('label'=>UserModule::t('Manage Profile Field'), 'type'=>'success', 'url'=>array('profileField/admin')),
            array('label'=>UserModule::t('List User'), 'type'=>'success', 'url'=>array('/user')),
        ),
    ),
);
?>
<!--<h1>--><?php //echo UserModule::t("Create User"); ?><!--</h1>-->

<?php
//echo $this->renderPartial('/../../test');
//?>

<?php
echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>