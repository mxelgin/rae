<?php
$this->breadcrumbs=array(
//	(UserModule::t('Users'))=>array('admin'),
//	$model->username=>array('view','id'=>$model->id),
//	(UserModule::t('Update')),
    'Управление пользователями'=>array('/user'),
    'Изменить'
);

$this->titleBox = '<h3>Изменить</h3>';

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
//            array('label'=>UserModule::t('Create User'), 'type'=>'primary', 'url'=>array('create')),
            array(
                'label'=>UserModule::t('Save'),
                'type'=>'success',
                'buttonType'=>'submit',
                'url'=>array('/user'),
                'htmlOptions'=>array(
                    'onclick' => '$("#user-form").submit()',
                )
            ),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>UserModule::t('View User'), 'type'=>'success', 'url'=>array('view','id'=>$model->id)),
//            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
            array('label'=>UserModule::t('Manage Profile Field'), 'type'=>'success', 'url'=>array('profileField/admin')),
            array('label'=>UserModule::t('List User'), 'type'=>'success', 'url'=>array('/user')),
        ),
    ),
);
?>

<!--<h1>--><?php //echo  UserModule::t('Update User')." ".$model->id; ?><!--</h1>-->

<?php
	echo $this->renderPartial('_form', array('model'=>$model,'profile'=>$profile));
?>