<div class="form form-content">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>true,
	'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>

	<p class="note"><?php echo UserModule::t('Fields with <span class="required">*</span> are required.'); ?></p>

	<?php echo $form->errorSummary(array($model,$profile)); ?>

    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr class="">
            <th>
                <?php echo $form->labelEx($model,'username'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'username',array('size'=>20,'maxlength'=>20, 'class'=>'span6')); ?>
<!--                --><?php //echo $form->error($model,'username'); ?>
            </td>
        </tr>

        <tr class="">
            <th>
                <?php echo $form->labelEx($model,'password'); ?>
            </th>
            <td>
                <?php echo $form->passwordField($model,'password',array('size'=>60,'maxlength'=>128, 'class'=>'span6')); ?>
<!--                --><?php //echo $form->error($model,'password'); ?>
            </td>
        </tr>

        <tr class="">
            <th>
                <?php echo $form->labelEx($model,'email'); ?>
            </th>
            <td>
                <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>128, 'class'=>'span6')); ?>
<!--                --><?php //echo $form->error($model,'email'); ?>
            </td>
        </tr>

        <tr class="">
            <th>
                <?php echo $form->labelEx($model,'superuser'); ?>
            </th>
            <td>
                <?php echo $form->dropDownList($model,'superuser',User::itemAlias('AdminStatus'), array('class'=>'span3 offset3')); ?>
<!--                --><?php //echo $form->error($model,'superuser'); ?>
            </td>
        </tr>

        <tr class="">
            <th>
                <?php echo $form->labelEx($model,'status'); ?>
            </th>
            <td>
                <?php echo $form->dropDownList($model,'status',User::itemAlias('UserStatus'), array('class'=>'span3 offset3')); ?>
<!--                --><?php //echo $form->error($model,'status'); ?>
            </td>
        </tr>


<?php 
		$profileFields=$profile->getFields();
		if ($profileFields) {
			foreach($profileFields as $field) {
			?>
	<tr class="">
        <th>
		    <?php echo $form->labelEx($profile,$field->varname); ?>
        </th>
        <td>
		    <?php
            if ($widgetEdit = $field->widgetEdit($profile)) {
                echo $widgetEdit;
            } elseif ($field->range) {
                echo $form->dropDownList($profile,$field->varname,Profile::range($field->range));
            } elseif ($field->field_type=="TEXT") {
                echo CHtml::activeTextArea($profile,$field->varname,array('rows'=>6, 'cols'=>50));
            } else {
                echo $form->textField($profile,$field->varname,array('size'=>60,'maxlength'=>(($field->field_size)?$field->field_size:255), 'class'=>'span6'));
            }
             ?>
        </td>
<!--		--><?php //echo $form->error($profile,$field->varname); ?>
	</tr>
			<?php
			}
		}
?>

        </tbody>
    </table>


</div><!-- form -->

<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            foreach(array_reverse($this->menu) as $_btnMenu){
                $this->widget('bootstrap.widgets.TbButtonGroup', array(
                    'buttons' => $_btnMenu['buttons']
                ));
            }
            ?>
        </div>
    </div>
<?php endif; ?>
<?php $this->endWidget(); ?>