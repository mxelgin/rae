<?php $this->pageTitle=Yii::app()->name . ' - '.UserModule::t("Restore");
$this->breadcrumbs=array(
//	UserModule::t("Login") => array('/user/login'),
	UserModule::t("Restore")
);

$this->titleBox = '<h3>Восстановить</h3>';

?>

<!--<h1>--><?php //echo UserModule::t("Restore"); ?><!--</h1>-->

<?php if(Yii::app()->user->hasFlash('recoveryMessage')): ?>
<div class="success">
<?php echo Yii::app()->user->getFlash('recoveryMessage'); ?>
</div>
<?php else: ?>

<div class="form form-content">
<?php echo CHtml::beginForm(); ?>

	<?php echo CHtml::errorSummary($form); ?>

    <table class="detail-view table table-striped table-condensed">
        <tbody>
            <tr>
                <th>
                    <?php echo CHtml::activeLabel($form,'login_or_email'); ?>
                </th>
                <td>
                        <?php echo CHtml::activeTextField($form,'login_or_email') ?>
                </td>
            </tr>
        </tbody>
	</table>
    <p class="hint"><?php echo UserModule::t("Please enter your login or email addres."); ?></p>
<!--	<div class="row submit">-->
<!--		--><?php //echo CHtml::submitButton(UserModule::t("Restore")); ?>
<!--	</div>-->

</div><!-- form -->
    <div class="modal-footer">
        <?php echo CHtml::submitButton(UserModule::t("Restore"), array('class'=>'btn btn-success')); ?>
    </div>

<?php echo CHtml::endForm(); ?>

<?php endif; ?>