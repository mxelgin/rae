<div class="form">

<?php //$this->renderPartial('/_menu'); ?>

<?php $form=$this->beginWidget('CActiveForm'); ?>

    <div class="row">
        <?php echo $form->error($model, 'itemname'); ?>
    </div>
	<div class="row">
		<?php echo $form->dropDownList($model, 'itemname', $itemnameSelectOptions); ?>
        <?php echo CHtml::submitButton(Rights::t('core', 'Add'), array('class'=>'btn btn-success')); ?>
	</div>

<?php $this->endWidget(); ?>

</div>