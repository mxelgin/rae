<?php $this->breadcrumbs = array(
//	'Rights'=>Rights::getBaseUrl(),
//	Rights::t('core', 'Create :type', array(':type'=>Rights::getAuthItemTypeName($_GET['type']))),
    'Права'=>array('/rights'),
//    'Операции'=>'/rights/authItem/operations',
    'Создать'
);

$this->titleBox = '<h3>Создать</h3>';

?>

<?php //$this->renderPartial('/_menu'); ?>
<?php $this->renderPartial('/../../_common'); ?>

<div class="createAuthItem">

<!--	<h2>--><?php //echo Rights::t('core', 'Create :type', array(
//		':type'=>Rights::getAuthItemTypeName($_GET['type']),
//	)); ?><!--</h2>-->

	<?php $this->renderPartial('_form', array('model'=>$formModel)); ?>

</div>