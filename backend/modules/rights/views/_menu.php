<?php //$this->widget('zii.widgets.CMenu', array(
//	'firstItemCssClass'=>'first',
//	'lastItemCssClass'=>'last',
//	'htmlOptions'=>array('class'=>'actions'),
//	'items'=>array(
//		array(
//			'label'=>Rights::t('core', 'Assignments'),
//			'url'=>array('assignment/view'),
//			'itemOptions'=>array('class'=>'item-assignments'),
//		),
//		array(
//			'label'=>Rights::t('core', 'Permissions'),
//			'url'=>array('authItem/permissions'),
//			'itemOptions'=>array('class'=>'item-permissions'),
//		),
//		array(
//			'label'=>Rights::t('core', 'Roles'),
//			'url'=>array('authItem/roles'),
//			'itemOptions'=>array('class'=>'item-roles'),
//		),
//		array(
//			'label'=>Rights::t('core', 'Tasks'),
//			'url'=>array('authItem/tasks'),
//			'itemOptions'=>array('class'=>'item-tasks'),
//		),
//		array(
//			'label'=>Rights::t('core', 'Operations'),
//			'url'=>array('authItem/operations'),
//			'itemOptions'=>array('class'=>'item-operations'),
//		),
//	)
//));	?>
<?php

$btnTemplates = array(
    array('label'=>'Таблица баз данных', 'url' => '/configStatement','icon'=>' icon-th-large','authItemName'=>'ConfigStatement.Admin',),
    array('label'=>'Муниципальный округ', 'url' => '/configDirectMo','icon'=>' icon-th-large','authItemName'=>'ConfigStatement.Admin',),
);

$this->menu=array(
//    array(
//        'class' => 'bootstrap.widgets.TbButtonGroup',
//        'buttons' => array(
////            array('label'=>UserModule::t('Manage Users'), 'url'=>array('admin')),
//            array(
//                'label'=>'Сохранить',
//                'type'=>'primary',
//                'buttonType'=>'submit',
//                'url'=>array('/rights'),
//                'htmlOptions'=>array(
//                    'onclick' => '$("#rights-assigment-form").submit()',
//                )
//            ),
//        ),
//    ),
    array(
        'class'=>'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label' => 'Справочники',
                'type'=>'primary',
                'items' =>$btnTemplates
            ),
        ),
    ),
    array(
        'class'=>'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'class' => 'bootstrap.widgets.TbButton',
                'type'=>'primary',
                'label'=>UserModule::t('Users'),
                'url'=>array('/user'),
            ),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array(
                'label' => UserModule::t('Rights'),
                'type'=>'primary',
                'items' =>array(
                    array(
                        'label'=>Rights::t('core', 'Assignments'),
                        'type'=>'primary',
                        'url'=>array('assignment/view'),
                        'itemOptions'=>array('class'=>'item-assignments'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Permissions'),
                        'type'=>'primary',
                        'url'=>array('authItem/permissions'),
                        'itemOptions'=>array('class'=>'item-permissions'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Roles'),
                        'type'=>'primary',
                        'url'=>array('authItem/roles'),
                        'itemOptions'=>array('class'=>'item-roles'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Tasks'),
                        'type'=>'primary',
                        'url'=>array('authItem/tasks'),
                        'itemOptions'=>array('class'=>'item-tasks'),
                    ),
                    array(
                        'label'=>Rights::t('core', 'Operations'),
                        'type'=>'primary',
                        'url'=>array('authItem/operations'),
                        'itemOptions'=>array('class'=>'item-operations'),
                    ),
//                    array(
//                        'label'=>'Права',
//                        'type'=>'primary',
//                        'url'=>array('/rights'),
//                        'itemOptions'=>array('class'=>'item-operations'),
//                    ),
                )
            )
        ),
    ),
);
?>