<!--<div class="form">-->

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'rights-assigment-form',
    'enableAjaxValidation'=>true,
    'htmlOptions' => array('enctype'=>'multipart/form-data'),
));
?>

    <div class="row">
        <?php echo $form->error($model, 'itemname'); ?>
    </div>
	<div class="row">
	    <?php echo $form->dropDownList($model, 'itemname', $itemnameSelectOptions); ?>
	    <?php echo CHtml::submitButton(Rights::t('core', 'Assign'), array('class'=>'btn btn-success')); ?>
    </div>

<?php $this->endWidget(); ?>

<!--</div>-->