<?php $this->breadcrumbs = array(
//	'Rights'=>Rights::getBaseUrl(),
//	Rights::t('core', 'Assignments'),
    'Права'=>array('/rights'),
    'Назначения'
);

$this->titleBox = '<h3>Права</h3>';

?>

<?php //$this->renderPartial('/_menu'); ?>
<?php $this->renderPartial('/../../_common'); ?>

<div id="assignments">


<!--	<h2>--><?php //echo Rights::t('core', 'Assignments'); ?><!--</h2>-->
<div class="form-content">
	<p>
		<?php echo Rights::t('core', 'Here you can view which permissions has been assigned to each user.'); ?>
	</p>

    <?php $this->widget('bootstrap.widgets.TbGridView', array(
	    'dataProvider'=>$dataProvider,
//	    'template'=>"{items}\n{pager}{delete}",
	    'emptyText'=>Rights::t('core', 'No users found.'),
        'type'=>'striped bordered',
	    'columns'=>array(
    		array(
    			'name'=>'name',
    			'header'=>Rights::t('core', 'Name'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'name-column'),
    			'value'=>'$data->getAssignmentNameLink()',
    		),
    		array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Roles'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'role-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_ROLE)',
    		),
			array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Tasks'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'task-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_TASK)',
    		),
			array(
    			'name'=>'assignments',
    			'header'=>Rights::t('core', 'Operations'),
    			'type'=>'raw',
    			'htmlOptions'=>array('class'=>'operation-column'),
    			'value'=>'$data->getAssignmentsText(CAuthItem::TYPE_OPERATION)',
    		),
//            array(
//                'class'=>'bootstrap.widgets.TbButtonColumn',
//                'template'=>'{delete}',
//            ),
	    )
	)); ?>

</div>
