<?php $this->breadcrumbs = array(
//	'Rights'=>Rights::getBaseUrl(),
//	Rights::t('core', 'Assignments')=>array('assignment/view'),
//	$model->getName(),
    'Права'=>array('/rights'),
    'Изменить'
);

$this->titleBox = '<h3>Изменить</h3>';

?>

<?php //$this->renderPartial('/_menu'); ?>
<?php $this->renderPartial('/../../_common'); ?>


<div id="userAssignments" class="form-content">

<!--	<h2>--><?php //echo Rights::t('core', 'Assignments for :username', array(
//		':username'=>$model->getName()
//	)); ?><!--</h2>-->
	
	<div class="assignments span-12 first">

		<?php $this->widget('bootstrap.widgets.TbGridView', array(
			'dataProvider'=>$dataProvider,
			'template'=>'{items}',
            'type'=>'striped bordered',
			'emptyText'=>Rights::t('core', 'This user has not been assigned any items.'),
			'htmlOptions'=>array('class'=>'grid-view user-assignment-table mini'),
			'columns'=>array(
    			array(
    				'name'=>'name',
    				'header'=>Rights::t('core', 'Name'),
    				'type'=>'raw',
    				'htmlOptions'=>array('class'=>'name-column'),
    				'value'=>'$data->getNameText()',
    			),
    			array(
    				'name'=>'type',
    				'header'=>Rights::t('core', 'Type'),
    				'type'=>'raw',
    				'htmlOptions'=>array('class'=>'type-column'),
    				'value'=>'$data->getTypeText()',
    			),
    			array(
    				'header'=>'&nbsp;',
    				'type'=>'raw',
    				'htmlOptions'=>array('class'=>'actions-column'),
    				'value'=>'$data->getRevokeAssignmentLink()',
    			),
			)
		)); ?>

	</div>

<!--	<div class="add-assignment span-11 last">-->

<!--		<h3>--><?php //echo Rights::t('core', 'Assign item'); ?><!--</h3>-->

		<?php if( $formModel!==null ): ?>

			<div class="form">

				<?php $this->renderPartial('_form', array(
					'model'=>$formModel,
					'itemnameSelectOptions'=>$assignSelectOptions,
				)); ?>

			</div>

		<?php else: ?>

			<p class="info"><?php echo Rights::t('core', 'No assignments available to be assigned to this user.'); ?>

		<?php endif; ?>

<!--	</div>-->

</div>

<div class="row"></div>
<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            foreach(array_reverse($this->menu) as $_btnMenu){
                $this->widget('bootstrap.widgets.TbButtonGroup', array(
                    'buttons' => $_btnMenu['buttons']
                ));
            }
            ?>
        </div>
    </div>
<?php endif; ?>
