<?php $this->pageTitle=Yii::app()->name . ' - ' . MessageModule::t("Compose Message"); ?>
<?php $isIncomeMessage = $viewedMessage->receiver_id == Yii::app()->user->getId() ?>

<?php
	$this->breadcrumbs = array(
//		MessageModule::t("Messages"),
		($isIncomeMessage ? MessageModule::t("Входящие") : MessageModule::t("Отправленные")) => ($isIncomeMessage ? 'inbox' : 'sent'),
		CHtml::encode($viewedMessage->subject),
	);

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Написать', 'type' => 'success', 'url'=>array('/message/compose')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Отправленные', 'type' => 'success', 'url'=>array('/message/sent/sent')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
             array('label'=>'Входящие', 'type' => 'success', 'url'=>array('/message')),
        ),
    ),
);
?>

<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>
<?php $this->titleBox = "<h3>$viewedMessage->subject</h3>"; ?>

<div class="form-content">
<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'message-view-delete-form',
	'enableAjaxValidation'=>false,
	'action' => $this->createUrl('delete/', array('id' => $viewedMessage->id))
)); ?>
	<button id="btn_view_delete" class="btn danger hidden"><?php echo MessageModule::t("Delete") ?></button>
<?php $this->endWidget(); ?>

<?php //if ($isIncomeMessage): ?>
<!--	<h4 class="message-from">От: --><?php //echo $viewedMessage->getSenderName() ?><!--</h4>-->
<?php //else: ?>
<!--	<h4 class="message-to">Кому: --><?php //echo $viewedMessage->getReceiverName() ?><!--</h4>-->
<?php //endif; ?>
<!---->
<!--<h4 class="message-subject">Тема: --><?php //echo CHtml::encode($viewedMessage->subject) ?><!--</h4>-->
<!---->
<!--<span class="date">--><?php //echo date(Yii::app()->getModule('message')->dateFormat, strtotime($viewedMessage->created_at)) ?><!--</span>-->
<!---->
<!--<div class="message-body">-->
<!--	--><?php //echo nl2br(($viewedMessage->body)) ?>
<!--</div>-->

<!--<h2>--><?php //echo MessageModule::t('Reply') ?><!--</h2>-->
<!--<h4>Ответить</h4>-->
	<?php $form = $this->beginWidget('CActiveForm', array(
		'id'=>'message-view-reply-form',
		'enableAjaxValidation'=>false,
	)); ?>

	<?php echo $form->errorSummary($message); ?>

    <div>
        <?php echo $form->hiddenField($message,'receiver_id'); ?>
        <?php echo $form->error($message,'receiver_id'); ?>
    </div>

    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr>

            <?php if ($isIncomeMessage): ?>
                <th>От</th>
                <td>
                    <?php echo $viewedMessage->getSenderName() ?>
                </td>
            <?php else: ?>
                <th>Кому</th>
                <td>
                    <?php echo $viewedMessage->getReceiverName() ?>
                </td>
            <?php endif; ?>
        </tr>
        <tr>
            <th>Тема</th>
            <td>
                <?php echo CHtml::encode($viewedMessage->subject) ?>
            </td>
        </tr>
        <tr>
            <th>Дата</th>
            <td>
                <?php echo date(Yii::app()->getModule('message')->dateFormat, strtotime($viewedMessage->created_at)) ?>
            </td>
        </tr>
        <tr>
            <th>Текст</th>
            <td>
                <?php echo nl2br(($viewedMessage->body)) ?>
            </td>
        </tr>

        <tr>
            <th>Ответить</th>
            <td>
            </td>
        </tr>
        <tr>
            <th>Тема</th>
            <td>
<!--                --><?php //echo $form->labelEx($message,'subject'); ?>
                <?php echo $form->textField($message,'subject', array('class'=>'span6')); ?>
<!--                --><?php //echo $form->error($message,'subject'); ?>
            </td>
        </tr>
        <tr>
            <th>Текст</th>
            <td>
<!--                --><?php //echo $form->labelEx($message,'body'); ?>
                <?php echo $form->textArea($message,'body', array('class'=>'span6', 'rows'=>'7')); ?>
<!--                --><?php //echo $form->error($message,'body'); ?>
            </td>
        </tr>
        </tbody>
    </table>
<!---->
	<div class="row buttons">
<!--        <button id="btn_view_reply" class="btn danger  ">--><?php //echo CHtml::submitButton(MessageModule::t("Reply")); ?><!--</button>-->
		<?php echo CHtml::submitButton(MessageModule::t("Reply"), array('id'=>"btn_view_reply", 'class'=>'hidden')); ?>
	</div>


	<?php $this->endWidget(); ?>
</div>

<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons' => array( array(
//                            'buttonType'=>'submit',
                    'type'=>'success',
                    'label'=>'Ответить',
                    'htmlOptions'=>array('onclick' => '$("#message-view-reply-form").submit()'),
//                            'id'=>'content_sent',
                    //                        'url'=>array('delete/', array('id' => $viewedMessage->id)),
                    //                        'htmlOptions'=>array('value'=>'Delete Selected'),
                ))));
            ?>
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons' => array( array(
//                      'buttonType'=>'submit',
                    'type'=>'success',
                    'label'=>'Удалить',
                    'htmlOptions'=>array(
//                            'onclick' => '$("#message-view-delete-form").submit()'
                        'onclick' => 'js:bootbox.dialog("Вы уверены, что хотите удалить этот элемент?", [{"label":"Удалить","class":"btn-danger","callback":function() { $("#message-view-delete-form").submit(); }}, {"label":"Отмена","class":"btn"}]);'

                    ),
//                          'id'=>'content_sent',
                    //                      'url'=>array('delete/', array('id' => $viewedMessage->id)),
                    //                      'htmlOptions'=>array('value'=>'Delete Selected'),
                ))));
            ?>
        </div>
    </div>
<?php endif; ?>