<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Messages:inbox"); ?>
<?php
$this->breadcrumbs=array(
//    MessageModule::t("Сообщения"),
    MessageModule::t("Входящие"),
);

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Написать', 'type' => 'success', 'url'=>array('/message/compose')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Отправленные', 'type' => 'success', 'url'=>array('/message/sent/sent')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
             array('label'=>'Входящие', 'type' => 'success', 'url'=>array('/message')),
        ),
    ),
);
?>

<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>

<?php $this->titleBox = '<h3>Входящие</h3>'; ?>
<!--<h2>--><?php //echo MessageModule::t('Inbox'); ?><!--</h2>-->

<!--<h2>--><?php //echo MessageModule::t('Inbox'); ?><!--</h2>-->

<div class="form-content">
<?php if ($messagesAdapter->data): ?>
    <?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
        'id'=>'message-inbox-form',
        'enableAjaxValidation'=>false,
        'action' => $this->createUrl('delete/')
    )); ?>

    <table class="items table table-striped table-bordered">
        <thread>
            <tr>
                <th>От</th>
                <th>Тема</th>
                <th>Дата</th>
            </tr>
        </thread>
        <tbody>
        <?php foreach ($messagesAdapter->data as $index => $message): ?>
            <tr class="<?php echo $message->is_read ? 'read' : 'unread' ?>">
                <td>
                    <?php echo CHtml::checkBox("Message[$index][selected]"); ?>
                    <?php echo $form->hiddenField($message,"[$index]id"); ?>
                    <?php echo $message->getSenderName(); ?>
                </td>
                <td><a href="<?php echo $this->createUrl('view/', array('message_id' => $message->id)) ?>"><?php echo $message->subject ?></a></td>
                <td><span class="date"><?php echo date(Yii::app()->getModule('message')->dateFormat, strtotime($message->created_at)) ?></span></td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>



<!--    --><?php //$this->widget('bootstrap.widgets.TbGridView', array(
//        'id'=>'message-inbox-grid',
//        'dataProvider'=>$messagesAdapter,
////        'filter'=>$message,
//        'type'=>'striped bordered',
//        'columns'=>array(
//            array(
//                'class'          => 'CCheckBoxColumn',
//                'selectableRows' => 2,
//                'visible'        => true,
////                'id' => $messagesAdapter->id. '_selected',
//                'id' => '$data->id',
////                'value'=>'$data["id"]',
//            ),
//            array(
//                'name' => 'demo',
//                'type'=>'raw',
//                'header' => "Select",
//                'value' => 'CHtml::checkBox("email[]","",array("class"=>"check","value"=>$data->id))',
//            ),
//            array(
//                'name'=>'temp',
//                'type'=>'raw',
//                'value' =>'$data->id'
//            ),
//            array(
//                'name'=>'From',
//                'type'=>'raw',
//                'value' =>'$data->SenderName'
//            ),
//            array(
//                'name'=>'Subject',
//                'type'=>'raw',
//                'value' => '$data->subject'
//            ),
//            array(
//                'class'=>'bootstrap.widgets.TbButtonColumn',
//                'template'=>'{delete}',
//                'buttons' => array(
////                'view' => array(
////                    'url'=>'"/client/$data->id"',
////                ),
//                ),
//            ),
//        ),
//    )); ?>

<!--    --><?php //$this->widget('CLinkPager', array('pages' => $messagesAdapter->getPagination())) ?>
    <div class="pagination">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $messagesAdapter->getPagination(),
            'header' => '',
            'nextPageLabel' =>'→',
            'prevPageLabel' => '←',
            'firstPageLabel'=>'<<',
            'lastPageLabel'=>'>>',
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'htmlOptions' => array(
                'class' => '',
            )
        ))
        ?>
    </div>



<!--    <div class="row buttons">-->
<!--        --><?php //echo CHtml::submitButton(MessageModule::t("Delete Selected")); ?>
<!--    </div>-->
</div>

<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons' => array( array(
//                        'buttonType'=>'submit',
                    'type'=>'success',
                    'label'=>'Удалить',
                    'id'=>'content_sent',
//                        'htmlOptions'=>array('value'=>'Delete Selected'),
                    'htmlOptions'=>array(
                        'id'=>'delete',
                        'value'=>'Delete Selected',
                        'onclick' => 'js:bootbox.dialog("Вы уверены, что хотите удалить этот элемент?", [{"label":"Удалить","class":"btn-danger","callback":function() { $("#message-inbox-form").submit(); }}, {"label":"Отмена","class":"btn"}]);'
                    ),
                ))));
            ?>
        </div>
    </div>
<?php endif; ?>
    <?php $this->endWidget(); ?>



<?php endif; ?>

