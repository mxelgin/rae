<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Compose Message"); ?>
<?php
	$this->breadcrumbs=array(
//		MessageModule::t("Messages"),
		MessageModule::t("Написать"),
	);

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Написать', 'type' => 'success', 'url'=>array('/message/compose')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Отправленные', 'type' => 'success', 'url'=>array('/message/sent/sent')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
             array('label'=>'Входящие', 'type' => 'success', 'url'=>array('/message')),
        ),
    ),
);
?>

<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation'); ?>
<?php $this->titleBox = '<h3>Написать</h3>'; ?>

<!--<h2>--><?php //echo MessageModule::t('Compose New Message'); ?><!--</h2>-->

<div class="form-content">
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'message-form-compose',
		'enableAjaxValidation'=>false,
	)); ?>

<p class="help-block">Поля помеченные <span class="required">*</span> обязательны для заполнения.</p>

	<?php echo $form->errorSummary($model); ?>


    <table class="detail-view table table-striped table-condensed">
        <tbody>
        <tr class="odd">
            <th>Получатель</th>
            <td>
                <?php echo $form->dropDownList($model, 'receiver_id', CHtml::listData(Users::model()->findAll(), "id", "username")); ?>
            </td>
        </tr>
        <tr class="even">
            <th>Тема</th>
            <td>
<!--                --><?php //echo $form->labelEx($model,'subject'); ?>
                <?php echo $form->textField($model,'subject', array('class'=>'span6')); ?>
<!--                --><?php //echo $form->error($model,'subject'); ?>
            </td>
        </tr>
        <tr class="even">
            <th>Текст</th>
            <td>
<!--                --><?php //echo $form->labelEx($model,'body'); ?>
                <?php echo $form->textArea($model,'body', array('class'=>'span6', 'rows'=>7)); ?>
<!--                --><?php //echo $form->error($model,'body'); ?>
            </td>
        </tr>
        </tbody>
    </table>

</div>
<!--	<div>-->
<!--		--><?php //echo CHtml::submitButton(MessageModule::t("Send")); ?>
<!--	</div>-->

<?php $this->endWidget(); ?>

<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons' => array(
                    array(
                        'buttonType'=>'submit',
                        'type'=>'success',
                        'label'=>'Отправить',
                        'id'=>'content_sent',
                        'htmlOptions'=>array('onclick'=>'$("#message-form-compose").submit(); '),
                        )
                )
            )
        );
?>
        </div>
    </div>
<?php endif; ?>


<?php $this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_suggest'); ?>
