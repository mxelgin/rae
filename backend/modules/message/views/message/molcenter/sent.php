<?php $this->pageTitle=Yii::app()->name . ' - '.MessageModule::t("Messages:sent"); ?>
<?php
	$this->breadcrumbs=array(
//		MessageModule::t("Messages"),
		MessageModule::t("Отправленные"),
	);

$this->menu=array(
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Написать', 'type' => 'success', 'url'=>array('/message/compose')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
            array('label'=>'Отправленные', 'type' => 'success', 'url'=>array('/message/sent/sent')),
        ),
    ),
    array(
        'class' => 'bootstrap.widgets.TbButtonGroup',
        'buttons' => array(
             array('label'=>'Входящие', 'type' => 'success', 'url'=>array('/message')),
        ),
    ),
);
?>

<?php //$this->renderPartial(Yii::app()->getModule('message')->viewPath . '/_navigation') ?>
<?php $this->titleBox = '<h3>Отправленные</h3>'; ?>

<!--<h2>--><?php //echo MessageModule::t('Sent'); ?><!--</h2>-->

<div class="form-content">
<?php if ($messagesAdapter->data): ?>
	<?php $form = $this->beginWidget('bootstrap.widgets.TbActiveForm', array(
		'id'=>'message-sent-form',
		'enableAjaxValidation'=>false,
		'action' => $this->createUrl('delete/')
	)); ?>

    <table class="items table table-striped table-bordered">
        <thread>
            <tr>
                <th>Кому</th>
                <th>Тема</th>
                <th>Дата</th>
            </tr>
        </thread>
        <tbody>
            <?php foreach ($messagesAdapter->data as $index => $message): ?>
                <tr>
                    <td>
                        <?php echo CHtml::checkBox("Message[$index][selected]"); ?>
                        <?php echo $form->hiddenField($message,"[$index]id"); ?>
                        <?php echo $message->getReceiverName() ?>
                    </td>
                    <td><a href="<?php echo $this->createUrl('view/', array('message_id' => $message->id)) ?>"><?php echo $message->subject ?></a></td>
                    <td><span class="date"><?php echo date(Yii::app()->getModule('message')->dateFormat, strtotime($message->created_at)) ?></span></td>
                </tr>
            <?php endforeach ?>
        </tbody>
	</table>

<!--    --><?php //$this->widget('CLinkPager', array('pages' => $messagesAdapter->getPagination())) ?>

    <div class="pagination">
        <?php
        $this->widget('CLinkPager', array(
            'pages' => $messagesAdapter->getPagination(),
            'header' => '',
            'nextPageLabel' =>'→',
            'prevPageLabel' => '←',
            'firstPageLabel'=>'<<',
            'lastPageLabel'=>'>>',
            'selectedPageCssClass' => 'active',
            'hiddenPageCssClass' => 'disabled',
            'htmlOptions' => array(
                'class' => '',
            )
        ))
        ?>
    </div>

<!--	<div class="row buttons">-->
<!--		--><?php //echo CHtml::submitButton(MessageModule::t("Delete Selected")); ?>
<!--	</div>-->


</div>

<?php if (!Yii::app()->request->isAjaxRequest): ?>
    <div class="form-actions">
        <div class="pull-right">
            <?php
            $this->widget('bootstrap.widgets.TbButtonGroup', array(
                'buttons' => array( array(
//                        'buttonType'=>'submit',
                    'type'=>'success',
                    'label'=>'Удалить',
                    'id'=>'content_sent',
                    'htmlOptions'=>array(
                        'value'=>'Delete Selected',
                        'onclick' => 'js:bootbox.dialog("Вы уверены, что хотите удалить этот элемент?", [{"label":"Удалить","class":"btn-danger","callback":function() { $("#message-sent-form").submit(); }}, {"label":"Отмена","class":"btn"}]);'
                    ),
                ))));
            ?>
        </div>
    </div>
<?php endif; ?>

	<?php $this->endWidget(); ?>

<?php endif; ?>

