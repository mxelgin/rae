<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class CreateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        /**
         * Creates a new model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         */
        $model=new Pay();
        $model->article_id = $id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Pay'])){
            $model->attributes=$_POST['Pay'];

            if($model->save(true)){
                if(Yii::app()->request->isAjaxRequest){
                    echo 'success';
                    Yii::app()->end();
                }
                else {
                    $this->controller->redirect(array('view','id'=>$model->article_id));
                }
            }
        }
        if(Yii::app()->request->isAjaxRequest)
            $this->controller->renderPartial('_form',array('model'=>$model), false, true);
        else
            $this->controller->render('create',array('model'=>$model));
    }

}