<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class AdminAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        /**
         * Manages all models.
         */
        $model=new Author('search');
        $model->unsetAttributes();  // clear any default values
        $model->article_id = $id;
        if(isset($_GET['Author']))
            $model->attributes=$_GET['Author'];

        $this->controller->render('admin',array(
            'model'=>$model,
        ));
    }

}