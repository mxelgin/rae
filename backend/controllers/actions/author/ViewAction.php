<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        $this->controller->redirect(array('admin','id'=>$id));
    }

}