<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        /**
         * Updates a particular model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id the ID of the model to be updated
         */
        $model=$this->controller->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Key']))
        {
            $model->attributes=$_POST['Key'];
            if($model->save()) {
                if(Yii::app()->request->isAjaxRequest){
                    echo 'success';
                    Yii::app()->end();
                }
                else {
                    $this->controller->redirect(array("file/{$model->article_id}"));
                }
            }
        }
        if(Yii::app()->request->isAjaxRequest)
            $this->controller->renderPartial('_form',array('model'=>$model), false, true);

        else
            $this->controller->render('update',array('model'=>$model));
    }

}