<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class PayAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        $this->controller->layout='//layouts/column1';

        $model = $this->controller->loadModel($id);
        $this->controller->render('pay',array('model'=>$model));
    }

}