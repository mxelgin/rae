<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class CreateAction extends CAction
{
    public function newArticle(){
        $model=new Article;

        $model->name_ru = 'СРАВНИТЕЛЬНАЯ ОЦЕНКА ИМПЛАНТАНТОВ';
        $model->name_en = 'COMPARATIVE EVALUATION OF IMPLANTS';
        $model->index ='631.16:658.1+338.43';
        $model->shifr ='08.00.05';


        $author = new Author();
        $author->family_ru = 'Иванов';
        $author->name_ru = 'Иван';
        $author->lastname_ru = 'Иванович';
        $author->article_id = 0;
        $author->article = $model;
        $author->email = 'email@mail.ru';
        $author->work_ru = 'ФБГОУ ВПО Южно-Уральский государственный университет';
        $author->work_en = 'South Ural state university (national research university), Chelyabinsk';
        $author->position_ru = 'доцент';
        $authors[] = $author;

        $author = new Author();
        $author->family_ru = 'Иванов';
        $author->name_ru = 'Иван';
        $author->lastname_ru = 'Иванович';
        $author->article_id = 0;
        $author->article = $model;
        $author->email = 'email@mail.ru';
        $author->work_ru = 'ФБГОУ ВПО Южно-Уральский государственный университет';
        $author->work_en = 'South Ural state university (national research university), Chelyabinsk';
        $author->position_ru = 'доцент';
        $authors[] = $author;

        $model->authors = $authors;

        $resume = new Resume();
        $resume->resume_ru = "Здесь - резюме на русском языке. Здесь - резюме на русском языке.";
        $resume->resume_en = "Publication's short description here. Publication's short description here.";
        $resume->article_id = 0;
        $resume->article = $model;

        $resumes[] = $resume;

        $model->resumes = $resumes;

        $key = new Key();
        $key->key_ru = "ключевые, слова, через, запятую, ключевые, слова, через, запятую";
        $key->key_en = "keywords, separated, by, comma, keywords, separated, by, comma";
        $key->article_id = 0;
        $key->article = $model;

        $keys[] = $key;

        $model->keys = $keys;

        return $model;

    }
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $model=new Article;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Article'])){
            $model->attributes=$_POST['Article'];
            if($model->save()){
                if(Yii::app()->request->isAjaxRequest){
                    echo 'success';
                    Yii::app()->end();
                }
                else {
                    $this->controller->redirect(array('author/admin','id'=>$model->id));
                }
            }
        }
        if(Yii::app()->request->isAjaxRequest)
            $this->controller->renderPartial('_form',array('model'=>$model), false, true);
        else
            $this->controller->render('create',array('model'=>$model));
    }

}