<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class AdminAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        /**
         * Manages all models.
         */
        $model=new Article('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['Article']))
            $model->attributes=$_GET['Article'];

        $this->controller->render('admin',array(
            'model'=>$model,
        ));
    }

}