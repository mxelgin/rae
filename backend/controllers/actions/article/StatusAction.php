<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class StatusAction extends CAction
{

    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        $this->controller->layout='//layouts/column1';

//        $dataProvider=new CActiveDataProvider('Article');

        $model=new Article('search');
        $model->unsetAttributes();  // clear any default values
//        if(isset($_GET['Article']))
//            $model->attributes=$_GET['Article'];

        $model->status_id = $id;
//        $data = $this->getDirects();
//        $model->queue_type_id = $id;
//        $data['model'] = $model;
//        $this->render('queue', $data);

        $this->controller->render('status', array(
            'model'=>$model,
        ));
    }
}