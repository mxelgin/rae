<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class UpdateAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        $model=$this->controller->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['Article']))
        {
            $model->attributes=$_POST['Article'];
            if($model->save()) {
                if(Yii::app()->request->isAjaxRequest){
                    echo 'success';
                    Yii::app()->end();
                }
                else {
                    $this->controller->redirect(array('author/admin','id'=>$model->id));
                }
            }
        }
        if(Yii::app()->request->isAjaxRequest)
            $this->controller->renderPartial('_form',array('model'=>$model), false, true);

        else
            $this->controller->render('update',array('model'=>$model));
    }

}