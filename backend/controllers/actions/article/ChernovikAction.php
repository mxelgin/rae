<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class ChernovikAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run()
    {
        $this->controller->layout='//layouts/column1';

        $dataProvider=new CActiveDataProvider('Article');
        $this->controller->render('chernovik',array(
            'dataProvider'=>$dataProvider,
        ));
    }

}