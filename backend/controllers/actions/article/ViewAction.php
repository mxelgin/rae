<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        $model = $this->controller->loadModel($id);
        if(empty($model))
            $this->controller->redirect(array('create','id'=>$id));
        else
            $this->controller->redirect(array('update','id'=>$id));
    }

}