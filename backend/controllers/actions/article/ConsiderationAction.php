<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class ConsiderationAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        $this->controller->layout='//layouts/column1';

        $model=$this->controller->loadModel($id);

        if(isset($_POST['Article']))
        {
//            $model->attributes=$_POST['Article'];
            $model->status_id = Article::STATUS_CONSIDERATION;
            if($model->save()) {
                if(Yii::app()->request->isAjaxRequest){
                    echo 'success';
                    Yii::app()->end();
                }
                else {
                    $this->controller->redirect(array('article/consideration','id'=>$model->id));
                }
            }
        }
        $this->controller->render('consideration',array('model'=>$model));
    }

}