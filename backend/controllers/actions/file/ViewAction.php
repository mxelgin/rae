<?php
/**
 * Created by PhpStorm.
 * User: mxelgin
 * Date: 04.02.15
 * Time: 3:22
 */

class ViewAction extends CAction
{
    /**
     * What to do when this action will be called.
     *
     * Just render the `index` view file from current controller.
     */
    public function run($id)
    {
        /**
         * Displays a particular model.
         * @param integer $id the ID of the model to be displayed
         */
        $model = File::model()->findByAttributes(array('article_id'=>$id));
        if(empty($model))
            $this->controller->redirect(array('create','id'=>$id));
        else
            $this->controller->redirect(array('update','id'=>$model->id));
    }

}