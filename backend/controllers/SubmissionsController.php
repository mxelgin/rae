<?php

class SubmissionsController extends BackendController
{
	public function actionIndex()
	{
		$this->render('index');
	}
	public function actionChernovik()
	{
        $dataProvider=new CActiveDataProvider('Article');
        $this->render('chernovik',array(
            'dataProvider'=>$dataProvider,
        ));
	}
}