<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div id="content">
    <br/>
        <?php
        $step1 = array(
            array('active'=>$this->step == 1,
                'url'=> isset($this->article_id) ? "/article/{$this->article_id}" : "#",
                'label'=>'Данные публикации',
                'itemOptions' => array('class' => $this->step < 1 ? 'disabled' : ''),
                'content'=>$this->step == 1 ?
                        $this->renderPartial('//layouts/column2', array('content'=>$content), true, true) : null));
        $step2 = array(
            array('active'=>$this->step == 2,
                'url'=> isset($this->article_id) ? "/author/{$this->article_id}" : "#",
                'label'=>'Авторы',
                'itemOptions' => array('class' => $this->step < 2 ? 'disabled' : ''),
                'content'=>$this->step == 2 ?
                        $this->renderPartial('//layouts/column2', array('content'=>$content), true, true) : null));
        $step3 = array(
            array('active'=>$this->step == 3,
                'url'=> isset($this->article_id) ? "/resume/{$this->article_id}" : "#",
                'itemOptions' => array('class' => $this->step < 3 ? 'disabled' : ''),
                'label'=>'Резюме',
                'content'=>$this->step == 3 ?
                        $this->renderPartial('//layouts/column2', array('content'=>$content), true, true) : null));
        $step4 = array(
            array('active'=>$this->step == 4,
                'url'=> isset($this->article_id) ? "/key/{$this->article_id}" : "#",
                'label'=>'Ключевые слова',
                'itemOptions' => array('class' => $this->step < 4 ? 'disabled' : ''),
                'content'=>$this->step == 4 ?
                        $this->renderPartial('//layouts/column2', array('content'=>$content), true, true) : null));
        $step5 = array(
            array('active'=>$this->step == 5,
                'url'=> isset($this->article_id) ? "/file/{$this->article_id}" : "#",
                'label'=>'Приложенные файлы',
                'itemOptions' => array('class' => $this->step < 5 ? 'disabled' : ''),
                'content'=>$this->step == 5 ?
                        $this->renderPartial('//layouts/column2', array('content'=>$content), true, true) : null));

        $tabs = array();

        $tabs = array_merge($tabs, $step1);
        $tabs = array_merge($tabs, $step2);
        $tabs = array_merge($tabs, $step3);
        $tabs = array_merge($tabs, $step4);
        $tabs = array_merge($tabs, $step5);

        $this->widget('bootstrap.widgets.TbTabs', array(
            'type' => 'tabs',
            'placement' => 'left',
            'tabs'=>$tabs,
            'htmlOptions'=>array('class'=>'tabs_custom')
        ));

        ?>
</div>
<?php $this->endContent(); ?>