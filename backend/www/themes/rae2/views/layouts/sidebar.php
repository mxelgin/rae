<div id="sidebar">
<!--    -->
<?php
//
    $this->widget('bootstrap.widgets.TbMenu',array(
        'type'=>'list',
        'items'=>array( ),
    ));

    $this->widget('application.components.YiiSmartMenu',
        array(
            'partItemSeparator'=>'.',
            'upperCaseFirstLetter'=>true,
            //Same options used in CMenu
            'type'=>'list',
            'items'=>array(
                array('label'=>'Публикации', 'url' => '/submissions','icon'=>' icon-th-large','authItemName'=>'User.Admin',),
                array('label'=>'Настройки', 'authItemName'=>'User.Admin',),
                array('label'=>'Права доступа', 'url' => '/user','icon'=>' icon-th-large','authItemName'=>'User.Admin',),
            )
        )
    );
?>

</div>
<!-- sidebar -->
