<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="sidebar-nav-fixed">
    <?php $this->beginContent('//layouts/sidebar'); ?>
    <?php $this->endContent(); ?>
</div>
<div class="content content-fixed">
    <?php
    $this->beginWidget(
        'bootstrap.widgets.TbBox',
        array(
            'title' => $this->titleBox,
            'headerIcon' => 'icon-home',
            //'htmlOptions'=>array('style'=>'clear:none'),
            'htmlOptions'=>array('class'=>'content-tbbox-fluid'),
        )
    );
    ?>
    <div id="content">
		<?php echo $content; ?>
    </div><!-- content -->
    <?php
    $this->endWidget();
    ?>
</div>
<?php $this->endContent(); ?>
