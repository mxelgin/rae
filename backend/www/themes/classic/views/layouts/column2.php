<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="span-5 last">
    <div id="sidebar">
        <?php
        $this->beginWidget('zii.widgets.CPortlet', array(
//            'title'=>'',
        ));
        $this->widget('zii.widgets.CMenu',array(
            'items'=>array(
                array('label'=>'Шаблоны лагерей', 'url'=>array('/camp')),
                array('label'=>'Формы анкет заявлений', 'url'=>array('/entry')),
                array('label'=>'Таблицы базы данных', 'url'=>array('/'), 'items'=>StateMainController::$submenu),
                array('label'=>'Активные ДОУ', 'url'=>array('/')),
                array('label'=>'Сообщения', 'url'=>array('/'), 'items'=>MessageMainController::$submenu),
                array('label'=>'Отчетность', 'url'=>array('/report'), 'items'=>ReportMainController::$submenu),
                array('label'=>'Справочники', 'url'=>array('/directory'), 'items'=>DirectoryMainController::$submenu),
                array('label'=>'Настройки', 'url'=>array('/')),
            ),
            'htmlOptions'=>array('class'=>'operations2'),
        ));

        $this->endWidget();
        ?>
    </div><!-- sidebar -->
</div>
<div class="span-20">
	<div id="content">
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<div id="sidebar">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			'title'=>'Действия',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
	</div><!-- sidebar -->
</div>
<?php $this->endContent(); ?>