<?php

/**
 * This is the model class for table "File".
 *
 * The followings are the available columns in table 'File':
 * @property integer $id
 * @property string $file1
 * @property string $file2
 * @property string $file3
 * @property string $file4
 * @property string $file5
 * @property integer $article_id
 *
 * The followings are the available model relations:
 * @property Article $article
 */
class File extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'File';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('article_id', 'required'),
			array('article_id', 'numerical', 'integerOnly'=>true),
//			array('file1, file2, file3, file4, file5', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, file1, file2, file3, file4, file5, article_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'article' => array(self::BELONGS_TO, 'Article', 'article_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'file1' => 'Текст статьи с рисунками и таблицами',
			'file2' => 'Сканированная сторонняя рецензия №1 (доктора наук)',
			'file3' => 'Сканированная сторонняя рецензия №2 (доктора наук)',
			'file4' => 'Сканированная сопроводительное письмо (подписанное руководителем учреждения или первым автором)',
			'file5' => 'Сканированное экспертное заключение (о возможности публикации материалов в открытой печати)',
			'article_id' => 'Article',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('file1',$this->file1,true);
		$criteria->compare('file2',$this->file2,true);
		$criteria->compare('file3',$this->file3,true);
		$criteria->compare('file4',$this->file4,true);
		$criteria->compare('file5',$this->file5,true);
		$criteria->compare('article_id',$this->article_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return File the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function behaviors(){
        return array(
            // наше поведение для работы с файлом
            'uploadableFile1'=>array(
                'class'=>'application.extensions.UploadableFileBehavior',
                'attributeName'=>'file1',
                'fileTypes'=>'jpg',
                // конфигурируем нужные свойства класса UploadableFileBehavior
                // ...
            ),
            'uploadableFile2'=>array(
                'class'=>'application.extensions.UploadableFileBehavior',
                'attributeName'=>'file2',
                'fileTypes'=>'jpg',
                // конфигурируем нужные свойства класса UploadableFileBehavior
                // ...
            ),
            'uploadableFile3'=>array(
                'class'=>'application.extensions.UploadableFileBehavior',
                'attributeName'=>'file3',
                'fileTypes'=>'jpg',
                // конфигурируем нужные свойства класса UploadableFileBehavior
                // ...
            ),
            'uploadableFile4'=>array(
                'class'=>'application.extensions.UploadableFileBehavior',
                'attributeName'=>'file4',
                'fileTypes'=>'jpg',
                // конфигурируем нужные свойства класса UploadableFileBehavior
                // ...
            ),
            'uploadableFile5'=>array(
                'class'=>'application.extensions.UploadableFileBehavior',
                'attributeName'=>'file5',
                'fileTypes'=>'jpg',
                // конфигурируем нужные свойства класса UploadableFileBehavior
                // ...
            ),
        );
    }

}
