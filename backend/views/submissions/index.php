<?php
/* @var $this SubmissionsController */

$this->breadcrumbs=array(
	'Управление публикациями',
);
$this->titleBox = '<h3>Управление публикациями</h3>';
?>
<!--<br/>-->
<!--<div class="row">-->
<!--    <div class="span1"></div>-->
<!--    <div class="span11">-->
<!--<div class="menu_item_header">-->
<!--    Управление публикациями-->
<!--</div>-->
<!--<br/>-->

<div class="form-content">

<div class="media media_item">
    <a class="pull-left" href="#">
        <img class="media-object" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/add.png">
    </a>
    <div class="media-body">
        <a class="media-heading" href="/article/create">
            Добавить новую публикацию
        </a>
        <div class="media">
            Добавление новых материалов автором, материалы должны быть оформлены в соответствии с правилами для авторов журнала.
        </div>
    </div>
</div>

 <br/>

<div class="media media_item">
    <a class="pull-left" href="#">
        <img class="media-object" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/Tag.png">
    </a>
    <div class="media-body">
        <a class="media-heading" href="/article/status/0">
            Черновики (<?php echo count(Article::model()->findAllByAttributes(array('status_id' => Article::STATUS_CHERNOVIK))); ?>)
        </a>
        <div class="media">
            Материалы добавлены автором. Автор не завершил оформление, не оплатил, не отправил в редакцию.
        </div>
    </div>
</div>

<div class="media media_item">
    <a class="pull-left" href="#">
        <img class="media-object" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/Tag.png">
    </a>
    <div class="media-body">
        <a class="media-heading" href="/article/status/1">
            Отправленные в редакцию материалы (<?php echo count(Article::model()->findAllByAttributes(array('status_id' => Article::STATUS_CONSIDERATION))); ?>)
        </a>
        <div class="media">
            Материалы отправлены в редакцию, находятся на рассмотрении
        </div>
    </div>
</div>

<div class="media media_item">
    <a class="pull-left" href="#">
        <img class="media-object" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/Tag.png">
    </a>
    <div class="media-body">
        <a class="media-heading" href="/article/status/2">
            Возвращенные автору материалы (<?php echo count(Article::model()->findAllByAttributes(array('status_id' => Article::STATUS_CANCEL))); ?>)
        </a>
        <div class="media">
            Материалы возвращены автору для исправления ошибок оформления
        </div>
    </div>
</div>
<!--<div class="media media_item">-->
<!--    <a class="pull-left" href="#">-->
<!--        <img class="media-object" src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/img/Tag.png">-->
<!--    </a>-->
<!--    <div class="media-body">-->
<!--        <a class="media-heading" href="/article/status/3">-->
<!--            Принятые к публикации материалы (--><?php //echo count(Article::model()->findAllByAttributes(array('status_id' => Article::STATUS_PUBLICATION))); ?><!--)-->
<!--        </a>-->
<!--        <div class="media">-->
<!--            Материалы приняты для публикации в журнале-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<!--<div class="media media_item">-->
<!--    <a class="pull-left" href="#">-->
<!--        <img class="media-object" src="--><?php //echo Yii::app()->theme->baseUrl; ?><!--/img/Tag.png">-->
<!--    </a>-->
<!--    <div class="media-body">-->
<!--        <a class="media-heading">-->
<!--            Подписанные в журнал материалы (0)-->
<!--        </a>-->
<!--        <div class="media">-->
<!--            Материалы подписаны в журнал, известны выходные данные публикации, подготовка к размещению на сайте.-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->
<div class="media media_item">
    <a class="pull-left" href="#">
        <img class="media-object" src="<?php echo Yii::app()->theme->baseUrl; ?>/img/Tag.png">
    </a>
    <div class="media-body">
        <a class="media-heading" href="/article/status/3">
            Опубликованные материалы (<?php echo count(Article::model()->findAllByAttributes(array('status_id' => Article::STATUS_PUBLICATION))); ?>)
        </a>
        <div class="media">
            Материалы опубликованы в журнале, размещены на сайте.
        </div>
    </div>
</div>

</div>







<!--    </div>-->
<!--</div>-->