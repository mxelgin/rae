<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('key_ru')); ?>:</b>
	<?php echo CHtml::encode($data->key_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('key_en')); ?>:</b>
	<?php echo CHtml::encode($data->key_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('article_id')); ?>:</b>
	<?php echo CHtml::encode($data->article_id); ?>
	<br />


</div>