<?php
$this->breadcrumbs=array(
	'Direct Sciences'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List DirectScience','url'=>array('index')),
//	array('label'=>'Manage DirectScience','url'=>array('admin')),
);
?>

<h1>Create DirectScience</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>