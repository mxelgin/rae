<?php
$this->breadcrumbs=array(
	'Direct Sciences'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List DirectScience','url'=>array('index')),
//	array('label'=>'Create DirectScience','url'=>array('create')),
//	array('label'=>'View DirectScience','url'=>array('view','id'=>$model->id)),
//	array('label'=>'Manage DirectScience','url'=>array('admin')),
);
?>

<h1>Update DirectScience <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>