<?php if (Yii::app()->request->isAjaxRequest): ?>
<div class="modal-header">
	<a class="close" data-dismiss="modal">&times;</a>
	<h4><?php echo $model->isNewRecord ? 'Create Author' : 'Update Author #'.$model->id ?></h4>
</div>

<div class="modal-body">
<?php endif; ?>


<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'author-form',
	'enableAjaxValidation'=>false,
)); ?>

    <div class="form-actions">
        <?php
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-left'),
            'buttons'=>array(
                array(
                    'type'=>'primary',
                    'label'=>'Назад',
                    'url'=>"/author/{$model->article_id}",
//                    'htmlOptions'=>array('class'=>'disabled'),
                ),
            )
        ));
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-left'),
            'buttons'=>array(
                array(
                    'type'=>'primary',
                    'label'=>'Вперед',
                    'url'=>"#",
                    'htmlOptions'=>array('class'=>$model->isNewRecord ? 'disabled' : ''),
                ),
            )
        ));
        $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-right'),
            'buttons'=>array(
                array(
                    'buttonType'=>'submit',
                    'type'=>'primary',
                    'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
                ),
            )
        )); ?>
    </div>

	<?php echo $form->errorSummary($model); ?>

	<?php echo $form->textFieldRow($model,'family_ru',array('class'=>'span11','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'name_ru',array('class'=>'span11','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'lastname_ru',array('class'=>'span11','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'email',array('class'=>'span11','maxlength'=>64)); ?>

	<?php echo $form->textFieldRow($model,'work_ru',array('class'=>'span11','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'work_en',array('class'=>'span11','maxlength'=>256)); ?>

	<?php echo $form->textFieldRow($model,'position_ru',array('class'=>'span11','maxlength'=>64)); ?>

	<?php echo $form->hiddenField($model,'article_id',array('class'=>'span11')); ?>

	<?php if (!Yii::app()->request->isAjaxRequest): ?>
	<?php endif; ?>
<?php $this->endWidget(); ?>

<?php if (Yii::app()->request->isAjaxRequest): ?>
</div>

<div class="modal-footer">
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'type'=>'primary',
        'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить изменения',
        'url'=>'#',
		'htmlOptions'=>array(
			'id'=>'submit-'.mt_rand(),
			'ajax' => array(
				'url'=>$model->isNewRecord ? $this->createUrl('create') : $this->createUrl('update', array('id'=>$model->id)),
				'type'=>'post',
				'data'=>'js:$(this).parent().parent().find("form").serialize()',
				'success'=>'function(r){
					if(r=="success"){
						window.location.reload();
					}
					else{
						$("#TBDialogCrud").html(r).modal("show");
					}
				}', 
			),
		),
    )); ?>
    <?php $this->widget('bootstrap.widgets.TbButton', array(
        'label'=>'Закрыть',
        'url'=>'#',
        'htmlOptions'=>array(
			'id'=>'btn-'.mt_rand(),
			'data-dismiss'=>'modal'
		),
    )); ?>
</div>
<?php endif; ?>