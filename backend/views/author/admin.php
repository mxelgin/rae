<?php
$this->breadcrumbs=array(
	'Публикации'=>array('/submissions'),
//	'Авторы',
);

$this->menu=array(
//	array('label'=>'Назад','url'=>array("article/{$model->article_id}")),
//	array('label'=>'Вперед','url'=>array("resume/{$model->article_id}")),
//	array('label'=>'Create','url'=>array("author/create/{$model->article_id}")),
//	array('label'=>'Create Author', 'url'=>array("author/create/{$model->article_id}"), 'linkOptions'=>array(
//		'ajax' => array(
//			'url'=>$this->createUrl("author/create/{$model->article_id}"),
//			'success'=>'function(r){$("#TBDialogCrud").html(r).modal("show");}',
//		),
//	)),
);
$this->article_id = $model->article_id;

?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Назад',
                'url'=>"/article/{$model->article_id}"
            ),
        )));
     $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-left'),
            'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Вперед',
                'url'=>count($model->search()->getData())>0 ? "/resume/{$model->article_id}" : "",
                'htmlOptions'=> array('class' => count($model->search()->getData())==0 ? 'disabled':''),
            ),
        )));
     $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-right'),
            'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Добавить автора',
                'url'=>"/author/create/{$model->article_id}"
            ),
        )));
     ?>
</div>

<div class="form-content">
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'author-grid',
    'type'=>'striped bordered',
	'ajaxUpdate'=>false,
	'dataProvider'=>$model->search(),
//	'filter'=>$model,
    'htmlOptions'=>array('class'=>'grid-view table_custom'),
	'columns'=>array(
//		'id',
		'family_ru',
		'name_ru',
		'lastname_ru',
		'email',
		'work_ru',
        array('class'=>'bootstrap.widgets.TbButtonColumn'),
	),
)); ?>
</div>
<p/>