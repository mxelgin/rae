<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('family_ru')); ?>:</b>
	<?php echo CHtml::encode($data->family_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name_ru')); ?>:</b>
	<?php echo CHtml::encode($data->name_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastname_ru')); ?>:</b>
	<?php echo CHtml::encode($data->lastname_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_ru')); ?>:</b>
	<?php echo CHtml::encode($data->work_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('work_en')); ?>:</b>
	<?php echo CHtml::encode($data->work_en); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('position_ru')); ?>:</b>
	<?php echo CHtml::encode($data->position_ru); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('article_id')); ?>:</b>
	<?php echo CHtml::encode($data->article_id); ?>
	<br />

	*/ ?>

</div>