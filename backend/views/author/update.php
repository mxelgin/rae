<?php
$this->breadcrumbs=array(
	'Authors'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List Author','url'=>array('index')),
//	array('label'=>'Create Author','url'=>array('create')),
//	array('label'=>'View Author','url'=>array('view','id'=>$model->id)),
//    array('label'=>'Manage Author','url'=>array("author/{$model->article_id}")),
);

$this->article_id = $model->article_id;

?>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>