<?php
$this->breadcrumbs=array(
	'Direct Types'=>array('index'),
	'Create',
);

$this->menu=array(
//	array('label'=>'List DirectType','url'=>array('index')),
//	array('label'=>'Manage DirectType','url'=>array('admin')),
);
?>

<h1>Create DirectType</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>