<?php
$this->breadcrumbs=array(
	'Direct Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
//	array('label'=>'List DirectType','url'=>array('index')),
//	array('label'=>'Create DirectType','url'=>array('create')),
//	array('label'=>'View DirectType','url'=>array('view','id'=>$model->id)),
//	array('label'=>'Manage DirectType','url'=>array('admin')),
);
?>

<h1>Update DirectType <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>