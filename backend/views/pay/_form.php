<?php $form=$this->beginWidget('CActiveForm',array(
    'htmlOptions'=>array('enctype'=>'multipart/form-data', 'class'=>'form_custom'),
)); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-right'),
        'buttons'=>array(
            array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Назад',
                'url'=>"/submissions"
            ),
        )
    ));
    ?>

</div>

<div class="custom_fields">

    <div class="text">
        <h1>Оплата публикации</h1>
        <p>Вам необходимо:</p>
        <ul>
            <li>
                <p>
                    Оплатить издательские услуги в размере xxx рублей для физических лиц по казанным ниже реквизитам:
                </p>
            </li>
            <li>
                <p>
                    После оплаты необходимо загрузиь сканкопию платежного поручения или квитанции
                </p>
            </li>
        </ul>
    </div>
    <div class="field">
        <?php if($model->pay): ?>
            <p><?php echo CHtml::encode($model->pay); ?></p>
        <?php endif; ?>
        <p>Загрузка файла</p>
        <p>Разрешенные форматы файлов: jpg, gif. Максимальный размер файла 5Мб.</p>
        <p></p>
        <?php echo $form->labelEx($model,'pay'); ?>
        <?php echo $form->fileField($model,'pay'); ?>
        <?php echo $form->error($model,'pay'); ?>
    </div>
</div>

<?php $this->endWidget(); ?>
