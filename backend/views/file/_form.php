<?php $form=$this->beginWidget('CActiveForm',array(
    'htmlOptions'=>array('enctype'=>'multipart/form-data'),
)); ?>
<?php /* текстовое поле названия элемента */ ?>
<!--<div class="field">-->
<!--    --><?php //echo $form->labelEx($model,'title'); ?>
<!--    --><?php //echo $form->textField($model,'title'); ?>
<!--    --><?php //echo $form->error($model,'title'); ?>
<!--</div>-->

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-right'),
        'buttons'=>array(
            array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=>$model->isNewRecord ? 'Создать' : 'Сохранить',
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-left'),
            'buttons'=>array(
                array(
                    'type'=>'primary',
                    'label'=>'Назад',
                    'url'=>"/key/{$model->article_id}"
                ),
            )
        ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Вперед',
                'url'=>"#",
                'htmlOptions'=>array('class'=>'disabled'),
            ),
        )));
    ?>

</div>

<?php /* поле для загрузки файла */ ?>
<div class="custom_fields">

<div class="text">
    <p>
    Разрешенные форматы файлов: <strong>doc, docx, jpg, gif, txt, pdf, rtf, zip, rar.</strong>
    Максимальный размер файла 5 Мб.
    </p>
    <p>
    В структуру статьи <strong>(объем не более 5 стр.)</strong> должны входить: название статьи, название учреждения, где выполенена работа,
    реферат (резюме), ключевые слова на русском и английском языках, реферат, цель исследования, материал и методы исследования,
    результат исследования и их обуждения, выводы или заключение, список литературы <strong>(не менее 5 источников)</strong>, сведения о
    рецензентах. Нарушение данного правила ведет к автоматическому возвращению материалов работ для исправления!
    </p>
    <p>
    Работа объемом менее 5 стр., а так же имеющая список литературных источников менее 5 не является статьей (краткое сообщение)
    и в соответствии с <a href="#">правилами для авторов</a> к публикации не принимается!
    </p>
</div>
<div class="field">
    <?php if($model->file1): ?>
        <p><?php echo CHtml::encode($model->file1); ?></p>
    <?php endif; ?>
    <?php echo $form->labelEx($model,'file1'); ?>
    <?php echo $form->fileField($model,'file1'); ?>
    <?php echo $form->error($model,'file1'); ?>
</div>
<div class="field">
    <?php if($model->file2): ?>
        <p><?php echo CHtml::encode($model->file2); ?></p>
    <?php endif; ?>
    <?php echo $form->labelEx($model,'file2'); ?>
    <?php echo $form->fileField($model,'file2'); ?>
    <?php echo $form->error($model,'file2'); ?>
</div>
<div class="field">
    <?php if($model->file3): ?>
        <p><?php echo CHtml::encode($model->file3); ?></p>
    <?php endif; ?>
    <?php echo $form->labelEx($model,'file3'); ?>
    <?php echo $form->fileField($model,'file3'); ?>
    <?php echo $form->error($model,'file3'); ?>
</div>
<div class="field">
    <?php if($model->file4): ?>
        <p><?php echo CHtml::encode($model->file4); ?></p>
    <?php endif; ?>
    <?php echo $form->labelEx($model,'file4'); ?>
    <?php echo $form->fileField($model,'file4'); ?>
    <?php echo $form->error($model,'file4'); ?>
</div>
<div class="field">
    <?php if($model->file5): ?>
        <p><?php echo CHtml::encode($model->file5); ?></p>
    <?php endif; ?>
    <?php echo $form->labelEx($model,'file5'); ?>
    <?php echo $form->fileField($model,'file5'); ?>
    <?php echo $form->error($model,'file5'); ?>
</div>
</div>
<?php /* кнопка отправки */ ?>
<!--<div class="button">-->
<!--    --><?php //echo CHtml::submitButton($model->isNewRecord ? 'Создать' : 'Сохранить'); ?>
<!--</div>-->


<?php $this->endWidget(); ?>
