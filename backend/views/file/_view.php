<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id),array('view','id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file1')); ?>:</b>
	<?php echo CHtml::encode($data->file1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file2')); ?>:</b>
	<?php echo CHtml::encode($data->file2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file3')); ?>:</b>
	<?php echo CHtml::encode($data->file3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file4')); ?>:</b>
	<?php echo CHtml::encode($data->file4); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('file5')); ?>:</b>
	<?php echo CHtml::encode($data->file5); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('article_id')); ?>:</b>
	<?php echo CHtml::encode($data->article_id); ?>
	<br />


</div>