<?php
$this->breadcrumbs=array(
	'Resumes',
);

$this->menu=array(
	array('label'=>'Create Resume','url'=>array('create')),
	array('label'=>'Manage Resume','url'=>array('admin')),
);
?>

<h1>Resumes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
