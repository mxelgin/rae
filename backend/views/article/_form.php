<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'article-form',
	'enableAjaxValidation'=>false,
)); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Назад',
                'url'=>"",
                'htmlOptions'=>array('class'=>'disabled'),
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Вперед',
                'url'=> $model->isNewRecord ? "" : "/author/{$model->id}",
                'htmlOptions'=>array('class'=>$model->isNewRecord ? 'disabled' : ''),
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-right'),
        'buttons'=>array(
            array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=> 'Сохранить',
            ),
        )
    ));
    ?>
</div>

<p class="help-block">Возможность записать информацию появляется только после того, как вы укажете все необходимые данные.</p>
<?php echo $form->errorSummary($model); ?>

<br/>
<?php echo $form->textAreaRow($model,'name_ru',array('class'=>'span11','maxlength'=>256)); ?>

<?php echo $form->textAreaRow($model,'name_en',array('class'=>'span11','maxlength'=>256)); ?>

    <div class="row-fluid">
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'science_id', CHtml::listData(DirectScience::model()->findAll(),'id','name'),array('class'=>'span8','maxlength'=>64)); ?>
        </div>
        <div class="span6">
            <?php echo $form->dropDownListRow($model,'type_id',CHtml::listData(DirectType::model()->findAll(),'id','name'),array('class'=>'span8','maxlength'=>64)); ?>
        </div>
    </div>

<?php echo $form->textFieldRow($model,'index',array('class'=>'span11','maxlength'=>64)); ?>
<?php echo $form->textFieldRow($model,'shifr',array('class'=>'span11','maxlength'=>64)); ?>

<?php $this->endWidget(); ?>