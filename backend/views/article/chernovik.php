<?php
$this->breadcrumbs=array(
    'Articles',
);
$model = $dataProvider->model;
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'article-grid',
    'ajaxUpdate'=>false,
    'dataProvider'=>$dataProvider,
    'htmlOptions'=>array('class'=>'table table-bordered grid-view table_custom'),
//    'filter'=>$model,
    'columns'=>array(
        array('class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons'=>array
            (
                'email' => array
                (
                    'label'=>'Send an e-mail to this user',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
                    'url'=>'Yii::app()->createUrl("users/email", array("id"=>$data->id))',
                ),
            )),
        array('name'=>'Название статьи', 'value'=>'$data->name_ru'),
        array('name'=>'availability', 'type'=>'raw', 'value'=>'$data->availability' ),
        array('name'=>'pay', 'type'=>'raw', 'value'=>'$data->pay' ),
//        array('name'=>'status', 'type'=>'raw', 'value'=>'$data->status' ),
    )
));
?>