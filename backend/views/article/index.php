<?php
$this->breadcrumbs=array(
	'Articles',
);

$this->menu=array(
//	array('label'=>'Create Article','url'=>array('create')),
//	array('label'=>'Manage Article','url'=>array('admin')),
);
?>

    <div class="form-actions">
        <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
            'htmlOptions'=>array('class'=>'pull-right'),
            'buttons'=>array(
                array(
                    'type'=>'primary',
                    'label'=>'Создать',
                    'url'=>"/article/create"
                ),
                array(
                    'type'=>'primary',
                    'label'=>'Управление',
                    'url'=>"/article/admin"
                ),
            )
        )); ?>
    </div>
<!--<h1>Articles</h1>-->

<?php //$this->widget('bootstrap.widgets.TbListView',array(
//	'dataProvider'=>$dataProvider,
//	'itemView'=>'_view',
//)); ?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'article-grid',
    'ajaxUpdate'=>false,
    'dataProvider'=>$dataProvider,
    'htmlOptions'=>array('class'=>'table table-bordered grid-view table_custom'),
//    'filter'=>$model,
    'columns'=>array(
        'id',
        'name_ru',
        'name_en',
        'shifr',
        'index',
        'science_id',
        /*
        'type_id',
        */
        array('class'=>'bootstrap.widgets.TbButtonColumn'),
//		array(
//			'class'=>'bootstrap.widgets.TbButtonColumn',
//			'buttons' => array(
//				'update' => array(
//					'click'=>'function(){
//						var url = $(this).attr("href");
//						$.get(url, function(r){
//							$("#TBDialogCrud").html(r).modal("show");
//						});
//						return false;
//					}',
//				),
//				'view' => array(
//					'click'=>'function(){
//						var url = $(this).attr("href");
//						$.get(url, function(r){
//							$("#TBDialogCrud").html(r).modal("show");
//						});
//						return false;
//					}',
//				),
//			),
//		),
    ),
)); ?>