<div class="form-content">
<?php
$this->breadcrumbs=array(
    'Публикации'=>array('/submissions'),
//    'Черновики'
);
$this->widget('bootstrap.widgets.TbGridView',array(
    'id'=>'article-grid',
    'type'=>'striped bordered',
    'ajaxUpdate'=>false,
    'dataProvider'=>$model->search(),
    'htmlOptions'=>array('class'=>' grid-view table_custom'),
//    'filter'=>$model,
    'columns'=>array(
        array('class'=>'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}',
            'buttons'=>array
            (
                'email' => array
                (
                    'label'=>'Send an e-mail to this user',
                    'imageUrl'=>Yii::app()->request->baseUrl.'/images/email.png',
                    'url'=>'Yii::app()->createUrl("users/email", array("id"=>$data->id))',
                ),
            )),
        array('name'=>'Название статьи', 'value'=>'$data->name_ru'),
        array('name'=>'availability', 'type'=>'raw', 'value'=>'$data->availability'),
//            'visible'=> $model->status_id == Article::STATUS_CHERNOVIK),
        array('name'=>'pay', 'type'=>'raw', 'value'=>'$data->pay'),
//            'visible'=> $model->status_id == Article::STATUS_CHERNOVIK),
        'date_created',
        array('name'=>'status', 'type'=>'raw', 'value'=>'$data->status' ),
    )
));
?>
</div>