<?php
$this->breadcrumbs=array(
    'Articles',
);
?>

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
    'id'=>'article-form',
    'enableAjaxValidation'=>false,
    'htmlOptions'=>array( 'class'=>'form_custom'),
)); ?>

<div class="form-actions">
    <?php
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Назад',
                'url'=>"/article/status/0",
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-left'),
        'buttons'=>array(
            array(
                'type'=>'primary',
                'label'=>'Далее',
                'url'=>"/article/status/1",
//                'htmlOptions'=>array('class'=>'disabled'),
            ),
        )
    ));
    $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'htmlOptions'=>array('class'=>'pull-right'),
        'buttons'=>array(
            array(
                'buttonType'=>'submit',
                'type'=>'primary',
                'label'=> 'Опубликовать',
                'htmlOptions' => array('class' => ($model->status_id == Article::STATUS_PUBLICATION) ? 'hidden' : ''),
            ),
        )
    ));
    ?>
</div>
    <?php echo $form->hiddenField($model,'id',array('class'=>'span11')); ?>
<?php
    if ($model->status_id == Article::STATUS_PUBLICATION){


    ?>
    <h1>Ваш материал принят на рассмотрение</h1>
        <p>
            В личном портфеле Ваша публикация будет перемещена в раздел «Отправленные в редакцию
            материалы»
        </p>
<?
}
?>

<?php
if ($model->status_id == Article::STATUS_CONSIDERATION){


    ?>
    <h1>Ваш материал для публикации в журнале принят</h1>
<!--    <p>-->
<!--        В личном портфеле Ваша публикация будет перемещена в раздел «Отправленные в редакцию-->
<!--        материалы»-->
<!--    </p>-->
<?
}
?>
<?php $this->endWidget(); ?>