<?php
/**
 * Base class for the controllers in backend entry point of application.
 *
 * In here we have the behavior common to all backend routes, such as registering assets required for UI
 * and enforcing access control policy.
 *
 * @package YiiBoilerplate\Backend
 */
abstract class MainPublicationController extends BackendController
{
    public $layout='//layouts/publication';

    public $step = 1;

    public $article_id;

}
