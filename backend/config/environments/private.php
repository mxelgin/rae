<?php
/**
 * Specific config overrides for backend entry point at local developer workstation.
 */
return [
    // Backend entry point at local can afford Gii support (DO NOT ALLOW it on production!)
    'modules' => [
        'gii' => [
            'class' => 'system.gii.GiiModule',
            'ipFilters' => ['127.0.0.1'],
            'password' => '12345',
            'generatorPaths'=>array(
                'bootstrap.gii',
            ),
            // Password will be set in the local config (see `local.example.php`)
        ]
    ],
    'components' => [
        'db' => [
            'connectionString' => 'mysql:host=192.168.1.111;dbname=c3rae',
            'username' => 'c3rae',
            'password' => 'c3rae',
        ]
    ],
];
