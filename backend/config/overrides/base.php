    <?php
    /**
     * Base config overrides for backend application
     */
    //Yii::setPathOfAlias('booster', ROOT_DIR . '/backend/extensions/yiibooster');

    return [
        // So our relative path aliases will resolve against the `/backend` subdirectory and not nonexistent `/protected`
        'basePath' => 'backend',
        'name'=>'ТюмГМА',
        'theme' => 'rae1',

        'preload' => array('log','bootstrap'),
        'aliases' => array(
            //If you used composer your path should be
            'xupload' => 'vendor.asgaroth.xupload',
        //If you manually installed it
    //         'xupload' => 'ext.xupload'
        ),
        'import' => [
            'common.actions.*',
            'application.controllers.*',
            'application.controllers.actions.*',
            'application.models.*',
            'application.components.*',
            'application.models.validators.*',
            'application.modules.message.*',
            'application.modules.message.models.*',
            'application.modules.user.models.*',
            'application.modules.user.components.*',
            'application.modules.rights.*',
            'application.modules.rights.components.*',
            'ext.giix-components.*', // giix components
        ],
        'modules'=>array(
            // uncomment the following to enable the Gii tool
            'user'=>array(
                # encrypting method (php hash function)
                'hash' => 'md5',

                'defaultController' => 'admin',

                # send activation email
                'sendActivationMail' => true,

                # allow access for non-activated users
                'loginNotActiv' => false,

                # activate user on registration (only sendActivationMail = false)
                'activeAfterRegister' => false,

                # automatically login from registration
                'autoLogin' => true,

                # registration path
                'registrationUrl' => array('/user/registration'),

                # recovery password path
                'recoveryUrl' => array('/user/recovery'),

                # login form path
                'loginUrl' => array('/user/login'),

                # page after login
                'returnUrl' => array('/user/profile'),

                # page after logout
                'returnLogoutUrl' => array('/user/login'),

                'tableUsers' => 'AuthUsers',
                'tableProfiles' => 'AuthProfiles',
                'tableProfileFields' => 'AuthProfilesFields',
            ),
            'rights'=>array(
                'superuserName'=>'Admin', // Name of the role with super user privileges.
                'authenticatedName'=>'Authenticated',  // Name of the authenticated user role.
                'userIdColumn'=>'id', // Name of the user id column in the database.
                'userNameColumn'=>'username',  // Name of the user name column in the database.
                'enableBizRule'=>true,  // Whether to enable authorization item business rules.
                'enableBizRuleData'=>true,   // Whether to enable data for business rules.
                'displayDescription'=>true,  // Whether to use item description instead of name.
                'flashSuccessKey'=>'RightsSuccess', // Key to use for setting success flash messages.
                'flashErrorKey'=>'RightsError', // Key to use for setting error flash messages.

                'baseUrl'=>'/rights', // Base URL for Rights. Change if module is nested.
                'layout'=>'//layouts/column2',  // Layout to use for displaying Rights.
                'appLayout'=>'application.views.layouts.main', // Application layout.
                //            'cssFile'=>'rights.css', // Style sheet file to use for Rights.
                'install'=>false,  // Whether to enable installer.
                'debug'=>false,
            ),
            'message' => array(
                'userModel' => 'Users',
                'getNameMethod' => 'getFullName',
                'getSuggestMethod' => 'getSuggest',
                'viewPath' => '/message/molcenter',
            ),
        ),
    //    'controllerMap' => [
        // Overriding the controller ID so we have prettier URLs without meddling with URL rules
    //        'site' => 'BackendSiteController'
    //    ],
        'components' => [
            // Backend uses the YiiBooster package for its UI
    //        'bootstrap' => [
    //             `bootstrap` path alias was defined in global init script
    //            'class' => 'bootstrap.components.Bootstrap'
    //        ],
            'user'=>array(
                // enable cookie-based authentication
                'allowAutoLogin'=>true,

                'class'=>'RWebUser',
                // enable cookie-based authentication
                'allowAutoLogin'=>true,
                'loginUrl'=>array('/user/login'),
            ),
            'authManager'=>array(
                'class'=>'RDbAuthManager',
                'connectionID'=>'db',
                'itemTable'=>'AuthItem',
                'itemChildTable'=>'AuthItemChild',
                'assignmentTable'=>'AuthAssignment',
                'rightsTable'=>'AuthRights',
            ),

            // uncomment the following to enable URLs in path-format
            'bootstrap' => array(
                'class' => 'bootstrap.components.Bootstrap',
            ),

            'urlManager'=>array(
                'urlFormat'=>'path',
                'showScriptName'=>false,
                'rules'=>array(
                    '<controller:\w+>/<id:\d+>'=>'<controller>/view',
                    '<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
                    '<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                ),
            ),
            /*
            'db'=>array(
                'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
            ),
            */
            // uncomment the following to use a MySQL database

    //        'db'=>array(
    //            'enableProfiling'=>true,
    //            'enableParamLogging' => true,
    ////            'connectionString' => 'mysql:host=localhost;dbname=c3molcenter',
    //            'emulatePrepare' => true,
    //            'username' => 'c3molcenter',
    //            'password' => 'molcenter',
    //            'charset' => 'utf8',
    //        ),

            'errorHandler'=>array(
                // use 'site/error' action to display errors
                'errorAction'=>'site/error',
            ),
            'log'=>array(
                'class'=>'CLogRouter',
                'routes'=>array(
                    array(
                        'class'=>'CFileLogRoute',
                        'levels'=>'error, warning',
                    ),
                    //                array(
                    //                    'class'=>'CProfileLogRoute',
                    //                    'levels'=>'profile',
                    //                    'enabled'=>true,
                    //                ),
                    // uncomment the following to show log messages on web pages
                    //                array(
                    //                    'class' => 'CWebLogRoute',
                    ////                    'categories' => 'application',
                    //                    'levels'=>'error, warning, trace, profile, info',
                    //                ),
                    //                array(
                    //                    'class' => 'CEmailLogRoute',
                    //                    'categories' => 'error',
                    //                    'emails' => array('mxelgin@yandex.ru'),
                    //                    'sentFrom' => 'mxelgin@yandex.ru',
                    //                    'subject' => 'Error at YiiFramework.ru'
                    //                ),
                ),
            ),
        ],
        'params'=>array(
            // this is used in contact page
            'adminEmail'=>'mxelgin@yandex.ru',
        ),
        'sourceLanguage' => 'en',
        'language' => 'ru',
    ];